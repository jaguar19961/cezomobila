<?php
if (!function_exists('categories')) {
    function categories()
    {
        return \App\Models\Category::with(['lang', 'childs.lang', 'childs.childs'])->where('parent_id', 0)->get();
    }
}

if (!function_exists('instas')) {
    function instas()
    {
        return \App\Models\Insta::get();
    }
}

if (!function_exists('instatext')) {
    function instatext()
    {
        return \App\Models\AboutUs::find(4);
    }
}

if (!function_exists('translation')) {
    function translation($ro, $ru)
    {
        $curr_lang = app()->getLocale();
        $text = '';
        if ($curr_lang === 'ro') {
            $text = $ro;
        }

        if ($curr_lang === 'ru') {
            $text = $ru;
        }

        return $text;
    }
}
