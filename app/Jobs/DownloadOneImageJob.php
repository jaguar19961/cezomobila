<?php

namespace App\Jobs;

use App\Models\Product;
use App\Models\ProductSpecification;
use App\Models\Shoes;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

/**
 * Class DownloadOneImageJob
 * @package App\Jobs
 */
class DownloadOneImageJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var string
     */
    private $imageUrl;
    /**
     * @var int
     */
    private $shoesID;

    /**
     * Create a new job instance.
     *
     * @param string $imageUrl
     * @param int $shoesID
     */
    public function __construct(string $imageUrl, int $shoesID)
    {
        $this->imageUrl = $imageUrl;
        $this->shoesID = $shoesID;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $file = Image::make($this->imageUrl)->encode('jpg');
        $basePath = '/images/';
        $relativePath = date('Y') . '/' . date('m') . '/' . date('d') . '/';
        $fileName = sha1(microtime(true)) . '.jpg';
        $fullPath = $basePath . $relativePath . $fileName;
        Storage::disk('public')->put($fullPath, $file);
        $item = Product::find($this->shoesID);
        $item->image = '/storage'.$fullPath;
        $item->save();
    }

    public function generateUuidNumber() {
        $number = mt_rand(100000, 999999);
        if ($this->uuidNumberExists($number)) {
            return $this->generateUuidNumber();
        }
        return $number;
    }

    public function uuidNumberExists($number) {
        // query the database and return a boolean
        // for instance, it might look like this in Laravel
        return Product::where('locale_sku', $number)->exists();
    }
}
