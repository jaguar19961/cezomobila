<?php

namespace App\Jobs;

use App\Models\Product;
use App\Models\ProductSpecification;
use App\Models\Shoes;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Spatie\Async\Pool;
use function PHPUnit\Framework\isNull;

class DownloadImages implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    protected $categoryId;
    protected $furnizorId;

    public function __construct($categoryId, $furnizorId)
    {
        $this->categoryId = $categoryId;
        $this->furnizorId = $furnizorId;
    }

    public function handle()
    {
        $collections = Product::where('category_id', $this->categoryId)
            ->where('furnizor_id', $this->furnizorId)
            ->get();
        foreach ($collections as $item) {
            if (!empty($item->galleries)) {
                if ($item->image === null) {
                    dispatch(new DownloadOneImageJob($item->galleries, $item->id));
                }
            }
        }
    }
}
