<?php

namespace App\Imports;

use App\Models\Brand;
use App\Models\Category;
use App\Models\Color;
use App\Models\Product;
use App\Models\ProductSpecification;
use App\Models\Shoes;
use App\Models\ShoesCategory;
use App\Models\ShoesInterior;
use App\Models\ShoesMaterial;
use App\Models\ShoesSize;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithMappedCells;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithUpserts;
use Spatie\Async\Pool;

class ShoesImport implements ToCollection, WithHeadingRow, WithStartRow, WithChunkReading, WithBatchInserts, WithMultipleSheets, WithUpserts
{
    use Importable;

    protected $category_id;
    protected $furnizor_id;

    public function __construct($category_id, $furnizor_id)
    {
        $this->category_id = $category_id;
        $this->furnizor_id = $furnizor_id;
    }

    /**
     * @param Collection $rows
     */
    public function collection(Collection $rows)
    {
        foreach ($rows as $row) {
            $db = Product::where('sku', !empty($row['sku']) ? $row['sku'] : null)->first();
            if (!empty($db)) {
                $new = Product::where('sku', $db->sku)->first();
            } else {
                $new = new Product();
            }
            $new->name_ro = !empty($row['name_ro']) ? $row['name_ro'] : null;
            $new->name_ru = !empty($row['name_ru']) ? $row['name_ru'] : null;
            $new->complete_name_ro = self::generateName($row, 'ro');
            $new->complete_name_ru = self::generateName($row, 'ru');
            $new->price_buing = !empty($row['price_buing']) ? $row['price_buing'] : null;
            $new->price = !empty($row['price']) ? $row['price'] : null;
            $new->price_discount = !empty($row['price_discount']) ? $row['price_discount'] : null;
            $new->locale_sku = $this->generateUuidNumber();
            $new->sku = !empty($row['sku']) ? $row['sku'] : null;
            $new->galleries = !empty($row['galleries']) ? $row['galleries'] : null;
            $new->category_id = $this->category_id;
            $new->furnizor_id = $this->furnizor_id;
            $new->excel = 1;
            $new->offer_type_new = 1;
            $new->save();

            Product::find($new->id)->update([
                'slug' => Str::slug($row['name_ro'] . ' ' . $new['locale_sku']),
            ]);
        }
    }

    public function generateName($row, $lang)
    {
        if (!empty($row['antent_' . $lang])) {
            $category = $row['antent_' . $lang];
        } else {
            $categoryObj = Category::with(['transMany', 'lang'])->find($this->category_id);
            if ($categoryObj->transMany) {
                if (!empty($categoryObj->transMany->where('lang_id', $lang)->description)) {
                    $category = $categoryObj->transMany->where('lang_id', $lang)->description;
                } else {
                    if (!empty($categoryObj->transMany->where('lang_id', $lang)->name)) {
                        $category = $categoryObj->transMany->where('lang_id', $lang)->name;
                    }else{
                        $category = '';
                    }
                }
            } else {
                $category = '';
            }

        }
        $furnizorObj = Brand::find($this->furnizor_id);
        $brand = $furnizorObj['brand'];
        $model = $row['name_' . $lang];
        return $category . ' ' . $brand . ' ' . $model;
    }

    public function generateUuidNumber()
    {
        $number = mt_rand(100000, 999999);
        if ($this->uuidNumberExists($number)) {
            return $this->generateUuidNumber();
        }
        return $number;
    }

    public function uuidNumberExists($number)
    {
        // query the database and return a boolean
        // for instance, it might look like this in Laravel
        return Product::where('locale_sku', $number)->exists();
    }


    /**
     * @param $row
     */
    public function setSize($row)
    {
        $result = [];
        $sizes = [
            $row['size_1'],
            $row['size_2'],
            $row['size_3'],
            $row['size_4'],
            $row['size_5'],
            $row['size_6'],
            $row['size_7'],
        ];
        foreach ($sizes as $size) {
            if (!empty($size) && $size != null && $size != " ") {
                $result[] = $size;
            }
        }
        return $result;
    }

    public function setSizeDb($row, $id)
    {
        $sizes = [
            $row['size_1'],
            $row['size_2'],
            $row['size_3'],
            $row['size_4'],
            $row['size_5'],
            $row['size_6'],
            $row['size_7'],
        ];
        $shoes_sizes_ids = array_filter($sizes);
        foreach ($shoes_sizes_ids as $size) {
            if (!empty($size) && $size != null && $size != " ") {
                ShoesSize::create([
                    'shoes_id' => $id,
                    'value' => $size
                ]);
            }
        }
    }

    /**
     * @param $color
     * @return mixed
     */
    public function checkColor($color)
    {
        $item = Color::where('name', $color)->first();
        if (!empty($item)) {
            return $item->id;
        } else {
            $item = new Color();
            $item->name = $color;
            $item->save();
            return (int)$item->id;
        }
    }

    /**
     * @param $category
     * @return mixed
     */
    public function checkCategory($category)
    {
        $item = ShoesCategory::where('name', $category)->first();
        if (!empty($item)) {
            return $item->id;
        } else {
            $item = new ShoesCategory();
            $item->name = $category;
            $item->save();
            return (int)$item->id;
        }
    }


    public function checkMaterial($material)
    {
        $item = ShoesMaterial::where('name', $material)->first();
        if (!empty($item)) {
            return $item->id;
        } else {
            $item = new ShoesMaterial();
            $item->name = $material;
            $item->save();
            return (int)$item->id;
        }
    }

    public function checkInterior($interior)
    {
        $item = ShoesInterior::where('name', $interior)->first();
        if (!empty($item)) {
            return $item->id;
        } else {
            $item = new ShoesInterior();
            $item->name = $interior;
            $item->save();
            return (int)$item->id;
        }
    }

    /**
     * @return int
     */

    public function startRow(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function chunkSize(): int
    {
        return 1000;
    }

    /**
     * @return int
     */
    public function batchSize(): int
    {
        return 1000;
    }

    /**
     * @return ShoesImport[]
     */
    public function sheets(): array
    {
        return [
            0 => $this,
        ];
    }

    /**
     * @return string|array
     */
    public function uniqueBy()
    {
        return 'size';
    }

}
