<?php

namespace App\Imports;

use App\Models\Brand;
use App\Models\Category;
use App\Models\Color;
use App\Models\Product;
use App\Models\SkuHistory;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithUpserts;

class ProductUpdateImport implements ToCollection, WithHeadingRow, WithStartRow, WithChunkReading, WithBatchInserts, WithMultipleSheets, WithUpserts, ShouldQueue
{
    use Importable;

    /**
     * @var
     */
    protected $furnizor_id;

    public function __construct($furnizor_id)
    {
        $this->furnizor_id = $furnizor_id;
    }

    /**
     * @param Collection $rows
     */
    public function collection(Collection $rows)
    {
        SkuHistory::truncate();
        $this->resetPriceByFurnizors();
        $array = [];
        foreach ($rows as $row) {
            $db = Product::where('sku', !empty($row['sku']) ? $row['sku'] : null)
                ->where('furnizor_id', $this->furnizor_id)
                ->first();
            if (empty($db)){
                $array[] = $row['sku'];
            }
            if (!empty($db)) {
                $new = Product::where('sku', $db->sku)->where('furnizor_id', $this->furnizor_id)->first();
                $new->complete_name_ro = self::generateName($db, 'ro');
                $new->complete_name_ru = self::generateName($db, 'ru');
                $new->price_buing = !empty($row['price_buing']) ? $row['price_buing'] : null;
                $new->price = !empty($row['price']) ? $row['price'] : null;
                $new->price_discount = !empty($row['price_discount']) ? $row['price_discount'] : null;
                $new->reseted_at = Carbon::now();
                $new->no_stock = 0;
                $new->excel = 1;
                $new->save();
            }
        }

        if ($array){
            SkuHistory::create([
                'array' => $array,
                'furnizor_id' => $this->furnizor_id,
            ]);
        }
    }

    public function generateName($row, $lang)
    {
        if (!empty($row['antent_' . $lang])) {
            $category = $row['antent_' . $lang];
        } else {
            $categoryObj = Category::with(['transMany', 'lang'])->find($row->category_id);
            if ($categoryObj->transMany) {
                if (!empty($categoryObj->transMany->where('lang_id', $lang)->first()->description)) {
                    $category = $categoryObj->transMany->where('lang_id', $lang)->first()->description;
                } else {
                    if (!empty($categoryObj->transMany->where('lang_id', $lang)->name)) {
                        $category = $categoryObj->transMany->where('lang_id', $lang)->name;
                    }else{
                        $category = '';
                    }
                }
            } else {
                $category = '';
            }

        }
        $furnizorObj = Brand::find($this->furnizor_id);
        $brand = $furnizorObj['brand'];
        $model = $row['name_' . $lang];
        return $category . ' ' . $brand . ' ' . $model;
    }

    public function resetPriceByFurnizors()
    {
        $furnizors = Product::where('furnizor_id', $this->furnizor_id)->get();
        foreach ($furnizors as $item) {
            $item->reseted_at = null;
            $item->no_stock = 1;
            $item->save();
        }
    }

    /**
     * @return int
     */
    public function generateUuidNumber()
    {
        $number = mt_rand(100000, 999999);
        if ($this->uuidNumberExists($number)) {
            return $this->generateUuidNumber();
        }
        return $number;
    }

    /**
     * @param $number
     * @return mixed
     */
    public function uuidNumberExists($number)
    {
        // query the database and return a boolean
        // for instance, it might look like this in Laravel
        return Product::where('locale_sku', $number)->exists();
    }

    /**
     * @return int
     */

    public function startRow(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function chunkSize(): int
    {
        return 1000;
    }

    /**
     * @return int
     */
    public function batchSize(): int
    {
        return 1000;
    }

    /**
     * @return ShoesImport[]
     */
    public function sheets(): array
    {
        return [
            0 => $this,
        ];
    }

    /**
     * @return string|array
     */
    public function uniqueBy()
    {
        return 'size';
    }

}
