<?php


namespace App\Facades\Meta;


use Illuminate\Support\Facades\Facade;

class MetaFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'meta';
    }
}

