<?php
namespace App\Facades\Meta\Tags;

class MetaName extends TagAbstract
{
    protected static $specials = ['canonical'];

    public static function tagDefault($key, $value)
    {
        if (!in_array($key, self::$specials, true)) {
            return '<meta name="'.$key.'" content="'.$value.'" />';
        }
    }
}
