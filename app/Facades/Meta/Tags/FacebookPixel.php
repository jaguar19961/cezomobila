<?php
namespace App\Facades\Meta\Tags;

class FacebookPixel extends TagAbstract
{
    protected static $available = [
        'brand', 'availability', 'catalog_id', 'category',
        'condition', 'custom_label_[0-4]', 'gender', 'item_group_id',
        'gtin', 'isbn', 'mfr_part_no', 'material', 'locale', 'price:amount',
        'price:currency', 'retailer_item_id', 'sale_price:amount',
        'sale_price:currency', 'sale_price_dates:start', 'sale_price_dates:end', 'id'
    ];

    public static function tagDefault($key, $value)
    {
        if (in_array($key, self::$available, true)) {

            return '<meta name="product:'. $key .'" content="'.$value.'" />';
        }
    }
}
