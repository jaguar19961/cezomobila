<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use App\Models\Category;
use App\Models\Invoice;
use App\Models\Order;
use App\Models\Product;
use App\Models\ProductSpecification;
use App\Models\Specification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class AxiosController extends Controller
{

    public function getCart(Request $request)
    {
        $data = collect($request->all());
        $carts = array();
        $total = 0;
        foreach ($data as $key => $value) {
            $exist = ProductSpecification::find($value['id']);
            if ($exist) {
                $item = ProductSpecification::with('product');
                $carts[] = array(
                    'item' => $item->where('id', $value['id'])->first(),
                    'quantity' => $value['quantity'],
                    'credit' => $value['credit'],
                    'disponibility' => true,
                );
                $total += $item->where('id', $value['id'])->first()['price'] * $value['quantity'];
            } else {
                $carts[] = array(
                    'item' => $value['id'],
                    'quantity' => $value['quantity'],
                    'credit' => $value['credit'],
                    'disponibility' => false,
                );
            }
        }


        return response(['status' => 200, 'carts' => $carts, 'total' => $total]);
    }

    public function typesProducts($type, $category)
    {

        if ($type === '2') {
            $products = Product::where('offer_type_populare', 1)->where('no_stock', 0);
        }

        if ($type === '3') {
            $products = Product::where('offer_type_new', 1)->where('no_stock', 0);
        }

        if ($category != 0) {
            $products = $products->where('category_id', $category);
        }

        $products = $products->get()
            ->groupBy('category_id');

        return response(['status' => 200, 'products' => $products]);
    }

    public function saveCheckout(Request $request)
    {
        $request->validate([
            'address' => 'required',
            'delivery_home' => 'required',
            'delivery_method' => 'required',
            'email' => 'required|email',
            'first_name' => 'required',
            'last_name' => 'required',
            'locality' => 'required',
            'phone' => 'required',
            'take_office' => 'required',
            'terms' => 'accepted',
            'products.*' => 'required|array',
        ]);

        $invoice = new Invoice();
        $invoice->address = $request->address;
        $invoice->delivery_home = $request->delivery_home;
        $invoice->delivery_method = $request->delivery_method;
        $invoice->email = $request->email;
        $invoice->first_name = $request->first_name;
        $invoice->last_name = $request->last_name;
        $invoice->locality = $request->locality;
        $invoice->phone = $request->phone;
        $invoice->take_office = $request->take_office;
        $invoice->terms = $request->terms;
        $invoice->status = 1;
        $invoice->total = $request->total;
        $invoice->delivery_price = $request->delivery;
        $invoice->save();

        foreach ($request->products as $product) {
            $order = new Order();
            $order->invoice_id = $invoice->id;
            $order->product_id = $product['id'];
            $order->sku = ProductSpecification::findOrFail($product['id'])->sku;
            $order->quantity = $product['quantity'];
            $order->price = ProductSpecification::findOrFail($product['id'])->price;
            $order->total = $product['quantity'] * ProductSpecification::findOrFail($product['id'])->price;
            $order->credit = $product['credit'];
            $order->save();
        }


        return response(['status' => 200]);
    }

}
