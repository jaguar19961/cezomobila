<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Upsell;
use Illuminate\Http\Request;

class UpsellController extends Controller
{
    public function get($id, $page_type)
    {
        if($page_type === 'product') {
            $products = Upsell::with('product')->where('show_in_product_id', $id)->get();
        }

        if($page_type === 'category') {
            $products = Upsell::with('product')->where('show_in_category_id', $id)->get();
        }
        return response(['data' => $products], 200);
    }

    public function store(Request $request)
    {
        $product_id = $request->get('product_id');
        $show_in_product_id = $request->get('combine_product_id');
        if (empty($product_id)) {
            return response('Nu sa gasit id-ul la produs', 200);
        }
        if (empty($show_in_product_id)) {
            return response('Nu sa gasit id-ul la produsul selectat', 200);
        }
        $products = Upsell::with('product')->where('show_in_product_id', $product_id)->get();
        if ($products->count() === 3) {
            return response('Maximum 3 products', 200);
        }

        if ($request->get('page_type') === 'product') {
            Upsell::create([
                'show_in_product_id' => $product_id,
                'product_id' => $show_in_product_id,
            ]);
        }

        if ($request->get('page_type') === 'category') {
            Upsell::create([
                'show_in_category_id' => $product_id,
                'product_id' => $show_in_product_id,
            ]);
        }

        return response('Success', 200);
    }

    public function delete($id)
    {
        Upsell::find($id)->delete();
        return response('Success', 200);
    }
}
