<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\ProductCombine;
use Illuminate\Http\Request;

class CombineProductsController extends Controller
{
    public function get($id)
    {
        $product = Product::where('id', $id)->first();

        $products = Product::where('similar_id', $product->similar_id)
        ->where('id', '<>', $id)->get();
        return response(['data' => $products], 200);


    }

    public function store(Request $request)
    {
        $ifHave = Product::find($request->get('product_id'));
        if (empty($ifHave->similar_id)) {
            $new = ProductCombine::create([
                'sku_similar' => $this->generateUuidNumber(),
            ]);
            $ifHave->similar_id = $new->id;
            $ifHave->save();

            $sku = $ifHave->similar_id;
            $product = Product::find($request->get('combine_product_id'));
            $product->similar_id = $sku;
            $product->save();
        } else {
            $sku = $ifHave->similar_id;
            $product = Product::find($request->get('combine_product_id'));
            $product->similar_id = $sku;
            $product->save();
        }


        return response('Success', 200);
    }


    public function generateUuidNumber()
    {
        $number = mt_rand(100000, 999999);
        if ($this->uuidNumberExists($number)) {
            return $this->generateUuidNumber();
        }
        return $number;
    }

    public function uuidNumberExists($number)
    {
        return ProductCombine::where('sku_similar', $number)->exists();
    }

    public function delete($id)
    {
        $prod = Product::find($id);
        $prod->similar_id = null;
        $prod->save();
        return response('Success', 200);
    }
}
