<?php

namespace App\Http\Controllers;

use App\Facades\Meta\MetaFacade;
use App\Models\AboutUs;
use App\Models\Blog;
use App\Models\Category;
use App\Models\Credit;
use App\Models\Gallery;
use App\Models\Invoice;
use App\Models\Local;
use App\Models\Page;
use App\Models\Product;
use App\Models\ProductCombine;
use App\Models\ProductSpecification;
use App\Models\Rate;
use App\Models\Recensy;
use App\Models\Specification;
use App\Models\Upsell;
use Illuminate\Http\Request;
use Spatie\SchemaOrg\Graph;
use Spatie\SchemaOrg\MultiTypedEntity;
use Spatie\SchemaOrg\Schema;

class FrontController extends Controller
{
    public function catalog(Request $request, $slug = null)
    {
        if (empty($request->get('category_id')) && empty($request->get('discount'))) {
            return abort('404');
        }

        $categories = Category::with('lang', 'childs.lang', 'attrib', 'parent')->where('parent_id', 0)->get();
        $brands = Specification::where('is_brand', 1)->first();
        $attributes = Specification::where('parent_id', 0)->where('is_brand', null)->orderBy('order_by', 'desc')->get();
        //needead data props
        $current_category = Category::with(['lang', 'childs', 'attrib', 'parent.lang'])->find($request->get('category_id'));
        if (!empty($slug)) {
            $current_category = Category::with(['lang', 'childs', 'attrib', 'parent.lang'])->where('slug', $slug)->first();
        }

        if ($current_category) {
            MetaFacade::set('title', $current_category->lang->name . ' - Cezomobila.md');
            MetaFacade::set('description', app()->getLocale() === 'ro' ? 'Cumpără ' . $current_category->lang->name . ' in rate 0% la un preț ieftin în Chișinău, Moldova' : 'Купить ' . $current_category->lang->name . ' в рассрочку 0% по низкой цене в Кишиневе, Молдова');
        } else {
            MetaFacade::set('title', app()->getLocale() === 'ro' ? 'Catalog produse la reducere - Cezomobila.md' : 'Каталог товаров со скидкой - Cezomobila.md');
            MetaFacade::set('description', app()->getLocale() === 'ro' ? 'Cumpără mobila in rate 0% la un preț ieftin în Chișinău, Moldova' : 'Купить мебель в рассрочку 0% по низкой цене в Кишиневе, Молдова');
        }


        //generate url for catalog
        $discount = $request->get('discount') ?? 0;
        $category = $request->get('category_id') ?? null;
        $price = $request->get('price');
        $sort = $request->get('order_by') ?? 'created_at';
        $search = $request->get('search') ?? null;
        $dinamic_filters = $request->get('dinamic_filters') ?? null;
        $page = $request->get('page') ?? 1;

        $maxPrice = 0;
        if (!empty($current_category->id)) {
            $maxPrice += Product::where('category_id', $current_category->id)->max('price');
        }
        //generated filters
        $props = [
            'discount' => $discount,
            'category_id' => $category,
            'price' => $price,
            'dinamic_filters' => $dinamic_filters,
            'order_by' => $sort,
            'search' => $search,
            'maxprice' => $maxPrice,
            'page' => $page
        ];

        $products_upsells = Upsell::where('show_in_category_id', $request->get('category_id'))->with('product')->get();
        $products_upsells_price = $products_upsells->sum('product.price');
        $products_upsells_discount_price = 0;
        foreach ($products_upsells as $item) {
            $products_upsells_discount_price += $item->product->price - $item->product->price_discount;
        }
        if ($products_upsells->count() > 0) {
            $products_upsells_offer_procent = floatval(round((($products_upsells_discount_price * 100) / $products_upsells->sum('product.price')) - 100, 2));
        } else {
            $products_upsells_offer_procent = 0;
        }
        $upsells_prices = [
            'products_upsells_price' => $products_upsells_price,
            'products_upsells_discount_price' => $products_upsells_discount_price,
            'products_upsells_offer_procent' => $products_upsells_offer_procent,
        ];


        return view('front.pages.catalog', compact('brands', 'categories', 'attributes', 'props', 'current_category', 'products_upsells', 'upsells_prices'));
    }

    public function product($slug)
    {
        $product = Product::where('slug', $slug)
            ->with('combines', 'color')
            ->withOut('lang')
            ->first();
        if (empty($product)) {
            return abort('404');
        }
        MetaFacade::set('title', $product->name . ' - Cezomobila.md');
        MetaFacade::set('description', app()->getLocale() == 'ro' ? 'Cumpără ' . $product->name . ' in rate 0% la un preț ieftin în Chișinău, Moldova' : 'Купить ' . $product->name . ' в рассрочку 0% по низкой цене в Кишиневе, Молдова');
        MetaFacade::set('image', asset($product->image));
//
        MetaFacade::set('product:id', $product->locale_sku);
        MetaFacade::set('product:gtin', $product->locale_sku);
        MetaFacade::set('product:value', $product->price);
        MetaFacade::set('product:price:amount', $product->price);
        MetaFacade::set('product:availability', $product->no_stock === 1 ? 'in stock' : 'out of stock');
        MetaFacade::set('product:price:currency', 'MDL');

        $credits = Credit::orderBy('order', 'asc')->where(function ($q) use ($product) {
            if ($product->credit_zero === 0) {
                $q->where('id', '!=', 1);
            }
        })->get();
        $specifications_collection = $product->productAttributes
            ->unique()
            ->groupBy('attribute_id');
        $specifications = [];
        foreach ($specifications_collection as $key => $item) {
            $specifications[Specification::find($key)->lang->name] = $item->unique('specification_id');
        }
        $reviews = Rate::where('product_id', $product->id)->with('product')->get();
        $similars = Product::where('no_stock', 0)
            ->where('category_id', $product->category_id)
            ->where('id', '<>', $product->id)
            ->inRandomOrder()
            ->take(4)->get();

        if ($product->similar_id != null) {
            $filterData = Product::where('similar_id', $product->similar_id)->get();
        } else {
            $filterData = Product::where('id', $product->id)->get();
        }

        $products_upsells = Upsell::where('show_in_product_id', $product->id)->with('product')->get();
        $products_upsells_price = $products_upsells->sum('product.price');
        $products_upsells_discount_price = 0;
        foreach ($products_upsells as $item) {
            $products_upsells_discount_price += $item->product->price - $item->product->price_discount;
        }
        if ($products_upsells->count() > 0) {
            $products_upsells_offer_procent = floatval(round((($products_upsells_discount_price * 100) / $products_upsells->sum('product.price')) - 100, 2));
        } else {
            $products_upsells_offer_procent = 0;
        }
        $upsells_prices = [
            'products_upsells_price' => $products_upsells_price,
            'products_upsells_discount_price' => $products_upsells_discount_price,
            'products_upsells_offer_procent' => $products_upsells_offer_procent,
        ];

        $localBusiness = new MultiTypedEntity();
        $localBusiness->product()
            ->name($product->name)
            ->sku($product->locale_sku)
            ->gtin($product->locale_sku)
            ->image(env('APP_URL') . $product->image)
            ->description($product->description)
            ->url(env('APP_URL') . '/product/' . $product->slug)
            ->price($product->price)
            ->identifier($product->locale_sku)
            ->priceCurrency('MDL')
            ->offers(
                Schema::offer()
                    ->name($product->name)
                    ->sku($product->locale_sku)
                    ->gtin($product->locale_sku)
                    ->image(env('APP_URL') . $product->image)
                    ->description($product->description)
                    ->url(env('APP_URL') . '/product/' . $product->slug)
                    ->price($product->price)
                    ->identifier($product->locale_sku)
                    ->priceCurrency('MDL')
            );

        return view('front.pages.product_page', compact('product', 'credits', 'specifications', 'reviews', 'similars', 'filterData', 'localBusiness', 'products_upsells', 'upsells_prices'));
    }

    public function cart()
    {
        MetaFacade::set('title', app()->getLocale() === 'ro' ? 'Cezomobila.md - cos' : 'Cezomobila.md - корзина');
        MetaFacade::set('description', app()->getLocale() === 'ro' ? 'Va multumim ca ati ajuns la etapa de comanda, cezomobila cea mai buna retea de mobila' : 'Спасибо, что дошли до стадии заказа, cezomobila лучшая мебельная сеть');
        return view('front.pages.card');
    }

    public function delivery()
    {
        $locals = Local::get();
        MetaFacade::set('title', app()->getLocale() === 'ro' ? 'Cezomobila.md - Livrare' : 'Cezomobila.md - Доставка');
        MetaFacade::set('description', app()->getLocale() === 'ro' ? 'Va multumim ca ati ajuns la etapa de comanda, cezomobila cea mai buna retea de mobila' : 'Спасибо, что дошли до стадии заказа, cezomobila лучшая мебельная сеть');
        return view('front.pages.delivery', compact('locals'));
    }

    public function confirm($id = null)
    {
        if ($id === null) {
            return redirect()->route('index');
        } else {
            $invoice = Invoice::where('invoice_id', $id)->first();

            if ($invoice === null) {
                return redirect()->route('index');
            } else {
                $invoice_id = $invoice->invoice_id;
                MetaFacade::set('title', app()->getLocale() === 'ro' ? 'Cezomobila.md - pagina de confirmare' : 'Cezomobila.md - страница подтверждения');
                MetaFacade::set('description', app()->getLocale() === 'ro' ? 'Va multumim pentru comanda, cezomobila cea mai buna retea de mobila din Moldova' : 'Спасибо за заказ, Cezomobila.md лучшая мебельная сеть в Молдове');
                return view('front.pages.confirm', compact('invoice_id'));
            }
        }
    }

    public function about()
    {
        MetaFacade::set('title', app()->getLocale() === 'ro' ? 'Cezomobila - despre noi' : 'Cezomobila - о нас');
        MetaFacade::set('description', app()->getLocale() === 'ro' ? 'Totul despre mobila, interior, cumpara la cele m-ai bune preturi din Moldova' : 'Все о мебели, интерьере купить по лучшим ценам в Молдове.');
        $model = AboutUs::findOrFail(2);
        return view('front.pages.about', compact('model'));
    }

    public function blog()
    {
        MetaFacade::set('title', app()->getLocale() === 'ro' ? 'Cezomobila - blog' : 'Cezomobila - блог');
        MetaFacade::set('description', app()->getLocale() === 'ro' ? 'Totul despre mobila, interior...' : 'Все о мебели, интерьере ...');
        $blogs = Blog::get();
        return view('front.pages.blog', compact('blogs'));
    }

    public function blogSingle($slug = null)
    {
        $blog = Blog::where('slug', $slug)->first();
        MetaFacade::set('title', 'Cezomobila - ' . $blog->lang->meta_name);
        MetaFacade::set('description', $blog->lang->meta_description);
        return view('front.pages.blog_single', compact('blog'));
    }

    public function contacts()
    {
        MetaFacade::set('title', app()->getLocale() === 'ro' ? 'Cezomobila - contacte' : 'Cezomobila - контакты');
        MetaFacade::set('description', app()->getLocale() === 'ro' ? 'Cezomobila unde ne gasiti, contacte, email si punctul pe harta totul facut ca sa ne gasiti m-ai repede' : 'Cezomobila, где вы можете найти нас, контакты, электронную почту и точку на карте, все сделано, чтобы нас найти.');
        $model = AboutUs::findOrFail(3);
        return view('front.pages.contacts', compact('model'));
    }

    public function info($slug = null)
    {
        if ($slug === null) {
            return redirect()->route('index');
        } else {
            $page = Page::where('slug', $slug)->first();
            if ($page === null) {
                return redirect()->route('index');
            } else {
                MetaFacade::set('title', 'Cezomobila - ' . $page->lang->meta_name);
                MetaFacade::set('description', $page->lang->meta_description);
                return view('front.pages.terms', compact('page'));
            }
        }
    }
}
