<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use App\Models\ProductSpecification;
use App\Models\Specification;
use Illuminate\Http\Request;

class CatalogController extends Controller
{
    public function getProducts(Request $request)
    {
        if ($request->order_by == 'asc' || $request->order_by == 'desc') {
            $sort = $request->order_by;
            $sorted = Product::orderBy('price', $sort);
            $sorted = $sorted->pluck('id')->toArray();
            $orderedIds = implode(',', $sorted);
        }


        $maxPrice = 0;
        $products = Product::where('no_stock', 0);

        if ($request->order_by == 'asc' || $request->order_by == 'desc') {
            $products = $products->orderByRaw(\DB::raw("FIELD(id, " . $orderedIds . " )"));
        }


        if ($request->order_by === 'created_at') {
            $products = $products->orderBy('created_at', 'desc');
        }

        if ($request->get('discount') == 1) {
            $products = $products->where('offer_type_promotie', 1);
        }

//        ############ CATEGORY FILTER ###############
        if ($request->get('category_id') != null && $request->get('category_id') != 'null') {
            $cat_id = $request->get('category_id');
            $category = Category::with(['lang', 'childs.lang', 'attrib'])->find($request->get('category_id'));
            if (count($category->childs) > 0) {
                $pluck = $category->childs->pluck('id')->toArray();
                $products = $products->whereIn('category_id', $pluck);
            } else {
                $products = $products->where('category_id', $cat_id);
            }
//        ############ PRICE  FILTER ###############
            $price = $request->get('price');
            $products = $products->whereBetween('price', [$price[0], $price[1]]);
//        ############ END PRICE  FILTER ###############
        }

        if ($request->order_by === 'created_at') {
            $products = $products->orderBy('created_at', 'asc');
        }


//        ############ END CATEGORY FILTER ###############

//        ############ GENERATE FILTER ###################
        $productsIds = $products->pluck('id');
        $scopeFilters = Specification::whereHas('productFilter', function ($query) use ($productsIds) {
            $query->whereIn('product_id', $productsIds)->with(['parent'])->distinct();
        })->get();
        $scopeFilters = $scopeFilters->groupBy('parent_id');
        $filters = array();
        foreach ($scopeFilters as $key => $filter) {
            $filters[Specification::findOrFail($key)->lang->name] = $filter;
        }

//        ############ END GENERATE FILTER ###############


//        ############ SPECIFICATION  FILTER ###############
//
        if (!empty(array_filter($request->dinamic_filters))) {
            $arr_ids = array_filter($request->dinamic_filters);
            $atr_ids = array_merge(...$arr_ids);
            $products = self::filterProducts($products, $atr_ids);
        }

        $maxPrice = $products->max('price');
        $products = $products->paginate(20);
        $products->makeHidden(['price_buing']);
        return response(['products' => $products, 'maxprice' => $maxPrice, 'filters' => $filters], 200);
    }

    public function filterProducts($products, $arr)
    {
        $groupByParent = Specification::whereIn('id', $arr)->get()->groupBy('parent_id');
        foreach ($groupByParent as $key => $active) {
            $filter = $active->pluck('id')->toArray();
            $products->whereHas('productAttributes', function ($query) use ($filter) {
                $query->whereIn('specification_id', $filter);
            });
        }

        return $products;
    }
}
