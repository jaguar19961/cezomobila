<?php

namespace App\Http\Controllers;

use App\Models\Color;
use App\Models\Translation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class ColorController extends Controller
{
    protected $model;
    protected $model_name;
    protected $translation;

    public function __construct(Color $model, Translation $translation)
    {
        $this->model = $model;
        $this->translation = $translation;
        $this->model_name = 'App\\Models\\Color::class';
    }

    public function index()
    {
        $model = $this->model->orderBy('created_at', 'desc')->get();
        return view('auth.pages.color.index', compact('model'));
    }

    public function create()
    {
        $model = $this->model::get();
        return view('auth.pages.color.create', compact('model'));
    }

    public function edit($id)
    {
        $model = $this->model->findOrFail($id);
        return view('auth.pages.color.show', compact('model'));
    }

    public function store(Request $request)
    {
        $exist = $this->model->where('slug', 'LIKE', $request->name['ro'])->first();
        if (!$exist) {
            $model = new $this->model();
            $model->save();
            foreach ($request->name as $key => $lang) {
                $arr = array(
                    'article_id' => $model->id,
                    'name' => $lang ? $lang : null,
                    'lang_id' => $key,
                    'model_name' => $this->model_name,
                );
                $this->translation->create($arr);
            }
            //----------
            $new = $this->model->findOrFail($model->id);
            $new->slug = Str::slug($request->name['ro']);
            $new->hex = $request->hex;
            if ($request->hasFile('image')) {
                $dir = '/img/category/';
                $extension = strtolower($request->file('image')->getClientOriginalExtension()); // get image extension
                $fileName = Str::random() . '.' . $extension; // rename image
                $request->file('image')->move(public_path($dir), $fileName);
                $new->image = $dir . $fileName;
            }
            $new->save();
            Session::flash('flash_message', 'Successfully Created!');

        }else{
            Session::flash('flash_message', 'This color Exists!');
        }
        return redirect()->back();

    }

    public function update(Request $request)
    {
        $model = $this->model::findOrFail($request->id);
        foreach ($request->name as $key => $lang) {
            $productNameTranslation = Translation::where('model_name', $this->model_name)->where('article_id', $request->id)->where('lang_id', $key)->first();
            $productNameTranslation->article_id = $request->id;
            $productNameTranslation->name = $request['name'][$key];
            $productNameTranslation->lang_id = $key;
            $productNameTranslation->model_name = $this->model_name;
            $productNameTranslation->save();
        }

        //----------
        //----------
        $new = $this->model->findOrFail($model->id);
        $new->slug = Str::slug($request->name['ro']);
        $new->hex = $request->hex;
        if ($request->hasFile('image')) {
            $dir = '/img/category/';
            $extension = strtolower($request->file('image')->getClientOriginalExtension()); // get image extension
            $fileName = Str::random() . '.' . $extension; // rename image
            $request->file('image')->move(public_path($dir), $fileName);
            $new->image = $dir . $fileName;
        }
        $new->save();

        Session::flash('flash_message', 'Successfully updated!');
        return redirect()->back();
    }

    public function destroy($id)
    {
        $model = $this->model->findOrFail($id);
        if ($model->productFilter->count() > 0){
            Session::flash('flash_message', 'This color have '. $model->productFilter->count() .' products, deletion not posible!');
            return redirect()->back();
        }else{
            $relation = $model->transMany;
            foreach ($relation as $key => $trans){
                $trans->delete();
            }
            $model->delete();

            Session::flash('flash_message', 'Successfully deleted!');
            return redirect()->back();
        }

    }
}
