<?php

namespace App\Http\Controllers;

use App\Facades\Meta\MetaFacade;
use App\Models\Gallery;
use App\Models\Product;
use App\Models\Rate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class IndexController extends Controller
{

    protected $products;

    public function __construct(Product $products)
    {
        $this->products = $products;
    }

    public function index()
    {
        MetaFacade::set('title', app()->getLocale() === 'ro' ? 'Cezomobila.md - mobilă pe placul tuturor.' : 'Cezomobila.md - мебель на любой вкус');
        MetaFacade::set('description', app()->getLocale() === 'ro' ? 'Internet magazin de mobilă cu livrare in Chișinau, Moldova. Cumpără mobilă pentru bucătărie, dormitor, living, birou, copii, hol, baie, terasă, ieftin, in rate 0%.' : 'Интернет-магазин мебели с доставкой по Кишиневу, Молдове. Купить мебель для кухни, спальни, гостиной, офиса, детской, прихожей, ванной, террасы недорого, в рассрочку 0%.');

        $promotions = Cache::remember('index.promotions', 30, function () {
            return $this->products->withoutRelations()->without(['productAttributes', 'productSpecifications', 'productSpecificationsAsc', 'rates', 'furnizor', 'combines'])->where('no_stock', 0)
                ->inRandomOrder()
                ->where('offer_type_promotie', 1)
                ->take(8)
                ->get();
        });

        $populars = Cache::remember('index.populars', 30, function () {
            return $this->products->withoutRelations()->without(['productAttributes', 'productSpecifications', 'productSpecificationsAsc', 'rates', 'furnizor', 'combines'])->where('no_stock', 0)
                ->inRandomOrder()
                ->take(8)
                ->get();
        });

        $new = Cache::remember('index.new', 30, function () {
            return $this->products->withoutRelations()->without(['productAttributes', 'productSpecifications', 'productSpecificationsAsc', 'rates', 'furnizor', 'combines'])->where('no_stock', 0)
                ->inRandomOrder()
                ->orderBy('created_at', 'desc')
                ->take(8)
                ->get();
        });


        $reviews = Rate::with('product')->where('state', 1)->take(6)->get();
        $galleries = Gallery::get();
        return view('front.pages.index', compact('promotions', 'reviews', 'galleries', 'populars', 'new'));
    }
}
