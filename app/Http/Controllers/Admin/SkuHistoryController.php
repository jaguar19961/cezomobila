<?php

namespace App\Http\Controllers\Admin;

use App\Models\SkuHistory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SkuHistoryController extends Controller
{
    public function index()
    {
        $model = SkuHistory::get();
        return view('auth.pages.history.index', compact('model'));
    }
}
