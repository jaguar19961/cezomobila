<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Credit;
use App\Models\CreditForm;
use App\Models\CreditsAttributePivot;
use App\Models\Product;
use App\Models\Translation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class CreditController extends Controller
{
    protected $model;
    protected $model_name;

    public function __construct(Credit $model, Translation $translation)
    {
        $this->model = $model;
        $this->translation = $translation;
        $this->model_name = 'App\\Models\\Credit::class';
    }

    public function index()
    {
        $model = $this->model->orderBy('created_at', 'desc')->get();
        return view('auth.pages.credit.index', compact('model'));
    }

    public function create()
    {
        return view('auth.pages.credit.create');
    }

    public function show($id)
    {
        $model = $this->model->findOrFail($id);
        return view('auth.pages.credit.show', compact('model'));
    }

    public function store(Request $request)
    {

        $input = $request->all();
        $model = $this->model::create($input);
        foreach ($request->name as $key => $lang) {
            $arr = array(
                'article_id' => $model->id,
                'name' => $lang ? $lang : null,
                'description' => $request['description'][$key],
                'lang_id' => $key,
                'model_name' => $this->model_name,
            );
            $this->translation->create($arr);
        }

        $new = $this->model->findOrFail($model->id);
        $new->slug = Str::slug($request->name['ro'] . '-' . $model->id);
        if ($request->hasFile('image')) {
            $dir = '/img/credit/';
            $extension = strtolower($request->file('image')->getClientOriginalExtension()); // get image extension
            $fileName = Str::random() . '.' . $extension; // rename image
            $request->file('image')->move(public_path($dir), $fileName);
            $new->image = $dir . $fileName;
        }
        $new->save();

        Session::flash('flash_message', 'Successfully Created!');
        return redirect()->back();
    }

    public function update(Request $request)
    {
        $model = $this->model::findOrFail($request->id);
        foreach ($request->name as $key => $lang) {
            $productNameTranslation = Translation::where('model_name', $this->model_name)->where('article_id', $request->id)->where('lang_id', $key)->first();
            $productNameTranslation->article_id = $request->id;
            $productNameTranslation->name = $request['name'][$key];
            $productNameTranslation->description = $request['description'][$key];
            $productNameTranslation->lang_id = $key;
            $productNameTranslation->model_name = $this->model_name;
            $productNameTranslation->save();
        }
        $new = $this->model->findOrFail($model->id);
        $new->slug = Str::slug($request->name['ro'] . '-' . $model->id);
        if ($request->hasFile('image')) {
            $dir = '/img/credit/';
            $extension = strtolower($request->file('image')->getClientOriginalExtension()); // get image extension
            $fileName = Str::random() . '.' . $extension; // rename image
            $request->file('image')->move(public_path($dir), $fileName);
            $new->image = $dir . $fileName;
        }
        $new->save();

        $this->model->find($model->id)->update($request->all());

        Session::flash('flash_message', 'Successfully updated!');
        return redirect()->back();
    }

    public function destroy($id)
    {
        $model = $this->model->findOrFail($id);
        $model->delete();

        Session::flash('flash_message', 'Successfully deleted!');
        return redirect()->back();
    }

    public function storeCredit(Request $request)
    {
        foreach (CreditsAttributePivot::where('credit_id', $request['credit_id'])->get() as $item) {
            $item->delete();
        }

        foreach ($request->credit as $key => $value) {
            $new = new CreditsAttributePivot();
            $new->credit_id = $request['credit_id'];
            $new->interest_rate = $value['interest_rate'];
            $new->term = $value['term'];
            $new->save();
        }
        return response(['status' => 200]);
    }

    public function storeCreditForm(Request $request)
    {
        $request->validate([
            'product_id' => 'required',
            'quantity' => 'required',
            'credit_id' => 'required',
            'avans' => 'required',
            'name' => 'required',
            'phone' => 'required',
        ]);

        CreditForm::create([
            'product_id' => $request->get('product_id'),
            'quantity' => $request->get('quantity'),
            'credit_id' => $request->get('credit_id'),
            'avans' => $request->get('avans'),
            'name' => $request->get('name'),
            'phone' => $request->get('phone'),
        ]);

        $data = [
            'data' => $request->all(),
            'product' => Product::findOrFail($request->get('product_id')),
            'credit' => Credit::findOrFail($request->get('credit_id')),
        ];

        $toEmail = 'cezomobila.md@gmail.com';
        Mail::send('front/new-credit', $data, function ($query) use ($toEmail) {
            $query->from('support@cezomobila.md', 'Cezomobila');
            $query->to($toEmail);
            $query->subject('Credit nou');
        });

        return response(['message' => 'Cererea pentru creditare a fost expediata cu success, asteptati apelul operatorului.'], 200);
    }
}
