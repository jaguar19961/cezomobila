<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Color;
use App\Models\Labels;
use App\Models\Product;
use App\Models\ProductGallery;
use App\Models\ProductOfferType;
use App\Models\ProductSpecification;
use App\Models\ProductSpecificationPivot;
use App\Models\Specification;
use App\Models\Translation;
use App\Models\ProductLabels;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

use Intervention\Image\Facades\Image;

class ProductsController extends Controller
{
    protected $model;
    protected $model_name;
    protected $translation;
    protected $brand;

    public function __construct(Product $model, Translation $translation, Brand $brand)
    {
        $this->model = $model;
        $this->translation = $translation;
        $this->brand = $brand;
        $this->model_name = 'App\\Models\\Product::class';
    }

    public function index()
    {
        $model = $this->model->orderBy('created_at', 'desc');
        $model = $model->paginate(20);
        $categories = Category::with(['lang', 'childs.lang', 'childs.childs', 'attrib', 'childs.attrib'])->where('parent_id', '<>', 0)->get();
        $furnizors = Brand::get();
        $data = [
            'search' => null,
            'category_id' => null,
            'furnizor_id' => null,
        ];
        return view('auth.pages.products.index', compact('model', 'categories', 'data', 'furnizors'));
    }

    public function search(Request $request)
    {
        $input = $request->search;
        $category_id = $request->get('category_id');
        $furnizor_id = $request->get('furnizor_id');
        $model = $this->model->orderBy('created_at', 'desc');
        if ($input != null) {
            $model = $model->where('locale_sku', $input)
                ->orWhere('name_ro', 'LIKE', "%{$input}%")
                ->orWhere('name_ru', 'LIKE', "%{$input}%")
                ->orWhereHas('furnizor', function ($qqq) use ($input) {
                    $qqq->where('name', $input);
                });
        }

        if ($category_id != 0) {
            $model = $model->where('category_id', $category_id);
        }

        if ($furnizor_id != 0) {
            $model = $model->where('furnizor_id', $furnizor_id);
        }
        $model = $model->get();
        $categories = Category::with(['lang', 'childs.lang', 'childs.childs', 'attrib'])->where('parent_id', '<>', 0)->get();
        $furnizors = Brand::get();
        $data = [
            'search' => $input,
            'category_id' => $category_id,
            'furnizor_id' => $furnizor_id,
        ];
        return view('auth.pages.products.index', compact('model', 'categories', 'data', 'furnizors'));
    }

    public function create()
    {
        $model = $this->model::get();
        $categories = Category::with(['lang', 'childs.lang', 'childs.childs', 'attrib', 'childs.attrib'])->where('parent_id', 0)->get();
        $specifications = Specification::where('parent_id', 0)->with('parent')->get();
        $offer_types = ProductOfferType::get();
        $furnizor = $this->brand->get();
        $labels = Labels::get();
        $colors = Color::get();
        return view('auth.pages.products.create', compact('model', 'categories', 'specifications', 'offer_types', 'furnizor', 'labels', 'colors'));
    }

    public function show($id)
    {
        $model = $this->model->findOrFail($id);
        $categories = Category::with(['lang', 'childs.lang', 'childs.childs','attrib'])->where('parent_id', 0)->get();
        $specifications = Specification::where('parent_id', 0)->with('parent')->get();
        $offer_types = ProductOfferType::get();
        $parent_id = Category::with(['lang', 'childs.lang', 'childs.childs', 'attrib'])->findOrFail($model->category_id)->parent_id;
        $parent_category = Category::with(['lang', 'childs.lang', 'childs.childs', 'attrib'])->with('childs')->findOrFail($parent_id);
        $furnizor = $this->brand->get();
        $labels = Labels::get();
        $colors = Color::get();
        return view('auth.pages.products.show', compact('parent_category', 'model', 'labels', 'categories', 'specifications', 'offer_types', 'furnizor', 'colors'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'category_id' => 'required',
            'product_name.*' => 'required',
            'description.*' => 'required',
            'price' => 'required|numeric',
            'sku' => 'required|unique:products'
        ]);
        $model = new $this->model();
        $model->save();

        $new = $this->model::findOrFail($model->id);
        $new->name_ro = $request->product_name['ro'];
        $new->name_ru = $request->product_name['ru'];

        $new->antent_ro = $request->antent['ro'];
        $new->antent_ru = $request->antent['ru'];

        $new->description_ro = $request->description['ro'];
        $new->description_ru = $request->description['ru'];

        $new->mounting_ro = $request->mounting['ro'];
        $new->mounting_ru = $request->mounting['ru'];

        $new->returning_ro = $request->returning['ro'];
        $new->returning_ru = $request->returning['ru'];

        $new->delivery_ro = $request->delivery['ro'];
        $new->delivery_ru = $request->delivery['ru'];

        $new->price = $request->price;
        $new->furnizor_id = $request->furnizor_id;
        $new->category_id = $request->category_id;
        $new->color_id = $request->color_id;
        $new->sku = $request->sku;
        $new->locale_sku = empty($model->locale_sku) ? $this->generateUuidNumber() : $model->locale_sku;
        $new->slug = Str::slug($request->product_name['ro'] . ' ' . $new->locale_sku);
        $new->price = $request->price;
        $new->price_discount = $request->price_discount;
        $new->price_buing = $request->price_buing;

        $new->offer_type_promotie = $request['offer_type_id']['promotie'];
        $new->offer_type_populare = $request['offer_type_id']['populare'];
        $new->offer_type_new = $request['offer_type_id']['noi'];
        $new->credit_zero = $request['offer_type_id']['credit_zero'];
        $new->no_stock = $request['offer_type_id']['no_stock'];

        //save Image
        if (!empty($request['image'])) {
            $new->image = $this->saveImage($request->image);
        }
        $new->save();

        //----------
        foreach ($request->sp as $k => $spec) {
            if (!empty($spec['selectedAttribute'])) {
                foreach ($spec['specs'] as $sp) {
                    $pivot = new ProductSpecificationPivot();
                    $pivot->attribute_id = $sp['specs'];
                    $pivot->specification_id = $sp['sub_specs'];
                    $pivot->product_id = $model->id;
                    $pivot->save();
                }
            }
        }

        return response(['status' => 200, 'prod_id' => $model->id]);
    }

    public function saveImage($file)
    {
        $file = Image::make($file)->encode('jpg');
        $basePath = '/images/';
        $relativePath = date('Y') . '/' . date('m') . '/' . date('d') . '/';
        $fileName = sha1(microtime(true)) . '.jpg';
        $fullPath = $basePath . $relativePath . $fileName;
        Storage::disk('public')->put($fullPath, $file);
        return '/storage' . $fullPath;
    }

    public function update(Request $request)
    {
//        dd($request->all());
        $request->validate([
            'category_id' => 'required',
            'product_name.*' => 'required',
            'description.*' => 'required',
            'price' => 'required|numeric',
        ]);
        $model = $this->model::findOrFail($request->product_id);

        $new = $this->model->findOrFail($model->id);
        if ($model->slug !== $request->slug){
            $new->slug = $request->slug;
        }

        $new->name_ro = $request->product_name['ro'];
        $new->name_ru = $request->product_name['ru'];

        $new->antent_ro = $request->antent['ro'];
        $new->antent_ru = $request->antent['ru'];

        $new->description_ro = $request->description['ro'];
        $new->description_ru = $request->description['ru'];

        $new->mounting_ro = $request->mounting['ro'];
        $new->mounting_ru = $request->mounting['ru'];

        $new->returning_ro = $request->returning['ro'];
        $new->returning_ru = $request->returning['ru'];

        $new->delivery_ro = $request->delivery['ro'];
        $new->delivery_ru = $request->delivery['ru'];

        $new->price = $request->price;
        $new->furnizor_id = $request->furnizor_id;
        $new->category_id = $request->category_id;
        $new->color_id = $request->color_id;
        $new->sku = $request->sku;
        $new->locale_sku = empty($model->locale_sku) ? $this->generateUuidNumber() : $model->locale_sku;
        $new->price = $request->price;
        $new->price_discount = $request->price_discount;
        $new->price_buing = $request->price_buing;

        $new->offer_type_promotie = $request['offer_type_id']['promotie'];
        $new->offer_type_populare = $request['offer_type_id']['populare'];
        $new->offer_type_new = $request['offer_type_id']['noi'];
        $new->credit_zero = $request['offer_type_id']['credit_zero'];
        $new->no_stock = $request['offer_type_id']['no_stock'];

        //save Image
        if ($model->image != $request['image'] && !empty($request['image'])) {
            $new->image = $this->saveImage($request->image);
        }
        $new->save();

        //----------

        $storeOldSpecIds = array();

        foreach (ProductSpecificationPivot::where('product_id', $request->product_id)->get() as $item) {
            $item->forceDelete();
        }

//        GENERATE SKUS CODE
        foreach ($request->sp as $k => $spec) {
            if (!empty($spec['selectedAttribute'])) {
                foreach ($spec['specs'] as $sp) {
                    $pivot = new ProductSpecificationPivot();
                    $pivot->attribute_id = $sp['specs'];
                    $pivot->specification_id = $sp['sub_specs'];
                    $pivot->product_id = $model->id;
                    $pivot->save();
                }
            }
        }

        return response(['status' => 200]);
    }

    private function galResetIds($oldId, $newId)
    {
        $gal = ProductGallery::where('product_id', $oldId)->get();
        foreach ($gal as $key => $item) {
            $item->product_id = $newId;
            $item->save();
        }
    }

    public function destroy($id)
    {
        $model = $this->model->findOrFail($id);
        $relation = $model->transMany;
        $spec = $model->productSpecifications;
        if (!empty($relation)) {
            foreach ($relation as $key => $trans) {
                $trans->delete();
            }
        }

        if (!empty($spec)) {
            foreach ($spec as $k => $specification) {
                $specification->delete();
            }
        }

        $model->delete();

        Session::flash('flash_message', 'Successfully deleted!');
        return redirect()->back();
    }

    public function specs(Request $request)
    {
        $specs = Specification::where('parent_id', $request->specs)->get();
        return response(['status' => 200, 'specs' => $specs]);
    }

    public function specsGrouped(Request $request)
    {
        $arr = collect($request->form);

        $specs = array();
        foreach ($arr as $key => $item) {
            foreach ($item['sub_specs'] as $i) {
                $specs[] += $i;
            }
        }
        $dataList = collect($specs)->unique();

        $specs = Specification::whereIn('id', $dataList)
            ->with(['parent', 'lang'])
            ->select(['parent_id', 'id'])
            ->get()
            ->groupBy(function ($item, $key) {
                return Specification::findOrFail($item->parent_id)->lang->name;
            });
//        dd($specs);
        $name = array();
        $da = array();
        foreach ($specs as $key => $item) {
            foreach ($item as $k => $kk) {
                $name[$key][] = Specification::findOrFail($kk->id)->lang->name;
            }
        }

        $result = array();
        foreach ($name as $kkk => $na) {
            $result[] = '<span>' . $kkk . ':' . implode(',', $na) . '</span>';
        }

        $result = implode($result);


        return response(['status' => 200, 'specs' => $result]);
    }

    public function getCat(Request $request)
    {
        $childs = Category::with(['lang', 'childs.lang', 'childs.childs', 'attrib'])->where('parent_id', $request->category_id)->get();

        return response(['status' => 200, 'childs' => $childs]);
    }

    public function uploadGallery(Request $request, $product_id)
    {
        if ($request->hasFile('file')) {
            $new = new ProductGallery();
            $file = \Intervention\Image\Facades\Image::make($request->file('file'))->encode('jpg');
            $basePath = '/images/';
            $relativePath = date('Y') . '/' . date('m') . '/' . date('d') . '/';
            $fileName = sha1(microtime(true)) . '.jpg';
            $fullPath = $basePath . $relativePath . $fileName;
            Storage::disk('public')->put($fullPath, $file);
            $new->name = $fileName;
            $new->url = '/storage' . $fullPath;
            $new->product_id = $product_id;
            $new->save();

            $fileList = ProductGallery::where('product_id', $product_id)->get();
            return response(['status' => 200, 'fileList' => $fileList]);
        }

    }

    public function delUploadGallery(Request $request)
    {
        $gal = ProductGallery::findOrFail($request->id);
        $path = public_path('/images' . '/products' . '/' . $request->name);
        File::delete($path);
        $gal->delete();

        return response(['status' => 200]);
    }

    public function generateUuidNumber()
    {
        $number = mt_rand(100000, 999999);
        if ($this->uuidNumberExists($number)) {
            return $this->generateUuidNumber();
        }
        return $number;
    }

    public function uuidNumberExists($number)
    {
        // query the database and return a boolean
        // for instance, it might look like this in Laravel
        return Product::where('locale_sku', $number)->exists();
    }
}
