<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CreditForm;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CreditFormController extends Controller
{
    public function index()
    {
        $model = CreditForm::with('product', 'credit')->get();
        return view('auth.pages.credit-form.index', compact('model'));
    }

    public function destroy($id)
    {
        CreditForm::find($id)->delete();
        Session::flash('flash_message', 'Successfully deleted!');
        return redirect()->back();
    }
}
