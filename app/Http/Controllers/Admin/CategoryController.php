<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\CategoryAtributePivot;
use App\Models\Specification;
use App\Models\Translation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class CategoryController extends Controller
{
    protected $model;
    protected $model_name;
    protected $translation;
    protected $specification;

    public function __construct(Category $model, Translation $translation, Specification $specification)
    {
        $this->model = $model;
        $this->translation = $translation;
        $this->specification = $specification;
        $this->model_name = 'App\\Models\\Category::class';
    }

    public function index($id)
    {
        $model = $this->model->orderBy('created_at', 'desc')->where('parent_id', $id)->get();
        $parent_name = $this->model->findOrFail($id);
        $attributes = $this->specification->where('parent_id', 0)->get();
        return view('auth.pages.category.index', compact('model', 'parent_name', 'attributes'));
    }

    public function create()
    {
        $model = $this->model::get();
        $categories = $this->model->where('parent_id', 0)->get();
        return view('auth.pages.category.create', compact('model', 'categories'));
    }

    public function show($id)
    {

        $model = $this->model->findOrFail($id);
        $categories = $this->model->where('parent_id', 0)->get();
        return view('auth.pages.category.show', compact('model', 'categories'));
    }

    public function storeAtributes(Request $request)
    {
//        dd($request->all());
        $oldAttr = CategoryAtributePivot::where('category_id', $request->category_id)->get();
        if ($oldAttr->count() > 0) {
            foreach ($oldAttr as $it) {
                $it->delete();
            }
        }
//        dd($request['attributes']);
        foreach ($request['attributes'] as $item) {
            $new = new CategoryAtributePivot();
            $new->category_id = $request['category_id'];
            $new->attribute_id = $item;
            $new->save();
        }
        return response(['status' => 200]);
    }

    public function store(Request $request)
    {

        $input = $request->all();
        $model = new $this->model();
        $model->save();
        foreach ($request->name as $key => $lang) {
            $arr = array(
                'article_id' => $model->id,
                'name' => $lang ? $lang : null,
                'description' => $request->get('description')[$key],
                'lang_id' => $key,
                'model_name' => $this->model_name,
            );
            $this->translation->create($arr);
        }

        //----------
        $new = $this->model->findOrFail($model->id);
        $new->slug = Str::slug($request->name['ro'] . '-' . $model->id);
        $new->parent_id = $request->parent_id != null ? $request->parent_id : 0;
        if ($request->hasFile('image')) {
            $dir = '/img/category/';
            $extension = strtolower($request->file('image')->getClientOriginalExtension()); // get image extension
            $fileName = Str::random() . '.' . $extension; // rename image
            $request->file('image')->move(public_path($dir), $fileName);
            $new->image = $dir . $fileName;
        }
        $new->save();

        Session::flash('flash_message', 'Successfully Created!');
        return redirect()->back();
    }

    public function update(Request $request)
    {
        $model = $this->model::findOrFail($request->id);
        foreach ($request->name as $key => $lang) {
            $productNameTranslation = Translation::where('model_name', $this->model_name)->where('article_id', $request->id)->where('lang_id', $key)->first();
            $productNameTranslation->article_id = $request->id;
            $productNameTranslation->name = $request['name'][$key];
            $productNameTranslation->description = $request['description'][$key];
            $productNameTranslation->lang_id = $key;
            $productNameTranslation->model_name = $this->model_name;
            $productNameTranslation->save();
        }

        //----------
        $new = $this->model->findOrFail($model->id);
        $new->slug = Str::slug($request->name['ro'] . '-' . $model->id);
        $new->parent_id = $request->parent_id != null ? $request->parent_id : 0;
        if ($request->hasFile('image')) {
            $dir = '/img/category/';
            $extension = strtolower($request->file('image')->getClientOriginalExtension()); // get image extension
            $fileName = Str::random() . '.' . $extension; // rename image
            $request->file('image')->move(public_path($dir), $fileName);
            $new->image = $dir . $fileName;
        }
        $new->save();

        Session::flash('flash_message', 'Successfully updated!');
        return redirect()->back();
    }

    public function destroy($id)
    {
        $model = $this->model->findOrFail($id);
        if ($model->products->count() > 0) {
            Session::flash('flash_message', 'This specification have ' . $model->products->count() . ' products, deletion not posible!');
            return redirect()->back();
        } else {
            $relation = $model->transMany;
            foreach ($relation as $key => $trans) {
                $trans->delete();
            }

            $model->delete();

            Session::flash('flash_message', 'Successfully deleted!');
            return redirect()->back();
        }
    }
}
