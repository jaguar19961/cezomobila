<?php

namespace App\Http\Controllers\Admin;

use App\Jobs\DownloadImages;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Storage;

class ProductUpdateController extends Controller
{
    protected $product;

    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    public function update(Request $request)
    {
        $request->validate([
            'file' => 'required',
            'furnizor_id' => 'required|numeric|min:0|not_in:0',
        ]);

        $fileName = md5(now()) . '.' . $request->file('file')->getClientOriginalExtension();
        Storage::disk('public')->put('excel/' . $fileName, $request->file('file')->get());
        Artisan::call('backup:update', ['filename' => $fileName, 'furnizor_id' => $request->get('furnizor_id')]);
        return redirect()->back();
    }

    public function show()
    {
        $categories = Category::where('parent_id', '<>', 0)->get();
        $manufacturer = Brand::get();
        return view('auth.pages.import.show', compact('categories', 'manufacturer'));
    }
}
