<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Brand;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class BrandController extends Controller
{
    protected $model;

    public function __construct(Brand $model)
    {
        $this->model = $model;
    }

    public function index()
    {
        $model = $this->model->orderBy('created_at', 'desc')->get();
        return view('auth.pages.brands.index', compact('model'));
    }

    public function create()
    {
        return view('auth.pages.brands.create');
    }

    public function show($id)
    {
        $model = $this->model->findOrFail($id);
        return view('auth.pages.brands.show', compact('model'));
    }

    public function store(Request $request)
    {
        $exist = $this->model->where('name', $request->name)->first();
        if (!$exist) {
            $input = $request->all();
            $model = $this->model::create($input);

            $new = $this->model->findOrFail($model->id);
            if ($request->hasFile('image')) {
                $dir = '/img/brand/';
                $extension = strtolower($request->file('image')->getClientOriginalExtension()); // get image extension
                $fileName = Str::random() . '.' . $extension; // rename image
                $request->file('image')->move(public_path($dir), $fileName);
                $new->image = $dir . $fileName;
            }
            $new->save();

            Session::flash('flash_message', 'Successfully Created!');
        }else{
            Session::flash('flash_message_error', 'This Brand Exist Please Fill Another!');
        }
        return redirect()->back();

    }

    public function update(Request $request)
    {
        $model = $this->model::findOrFail($request->id);
        $input = $request->all();

        $model->fill($input)->save();
        $new = $this->model->findOrFail($model->id);
        if ($request->hasFile('image')) {
            $dir = '/img/brand/';
            $extension = strtolower($request->file('image')->getClientOriginalExtension()); // get image extension
            $fileName = Str::random() . '.' . $extension; // rename image
            $request->file('image')->move(public_path($dir), $fileName);
            $new->image = $dir . $fileName;
        }
        $new->save();

        Session::flash('flash_message', 'Successfully updated!');
        return redirect()->back();
    }

    public function destroy($id)
    {
        $model = $this->model->findOrFail($id);
        $model->delete();

        Session::flash('flash_message', 'Successfully deleted!');
        return redirect()->back();
    }
}
