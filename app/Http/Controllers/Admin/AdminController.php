<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Script;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function index()
    {
        $script = Script::findOrFail(1);
        return view('auth.pages.dashboard', compact('script'));
    }

}
