<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use App\Models\ProductSpecification;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function search($search)
    {
        if ($search != '') {
            if (app()->getLocale() == 'ro') {
                $q = '%' . $search . '%';
                $products = Product::where('complete_name_ro', 'Like', '?')
                    ->orWhere('locale_sku', 'Like', '?')
                    ->orderByRaw("
    CASE WHEN complete_name_ro LIKE ? THEN 1
         WHEN locale_sku LIKE ? THEN 2
         ELSE 3 END
")->setBindings([$q, $q, $q, $q]);
            }else{
                $q = '%' . $search . '%';
                $products = Product::where('complete_name_ru', 'Like', '?')
                    ->orWhere('locale_sku', 'Like', '?')
                    ->orderByRaw("
    CASE WHEN complete_name_ru LIKE ? THEN 1
         WHEN locale_sku LIKE ? THEN 2
         ELSE 3 END
")->setBindings([$q, $q, $q, $q]);
            }

            $count = $products->count();
            $products = $products->take(30)->get();
        } else {
            $products = [];
            $count = 0;
        }

        return response(['status' => 200, 'result' => $products, 'count' => $count], 200);
    }

    public function newSearch($query)
    {
        if (app()->getLocale() == 'ro') {
            $q = '%' . $query . '%';
            $products = Product::where('complete_name_ro', 'Like', '?')
                ->orWhere('name_ro', 'Like', '?')
                ->orderByRaw("
    CASE WHEN complete_name_ro LIKE ? THEN 1
         WHEN name_ro LIKE ? THEN 2
         ELSE 3 END
")->setBindings([$q, $q, $q, $q])->take(20)->get();
        }else{
            $q = '%' . $query . '%';
            $products = Product::where('complete_name_ru', 'Like', '?')
                ->orWhere('name_ru', 'Like', '?')
                ->orderByRaw("
    CASE WHEN complete_name_ru LIKE ? THEN 1
         WHEN name_ru LIKE ? THEN 2
         ELSE 3 END
")->setBindings([$q, $q, $q, $q])->take(20)->get();
        }

        $productIds = $products->pluck('category_id')->unique();
        $categories = Category::whereIn('id', $productIds)
            ->where('parent_id', '!=', 0)
            ->where('parent_id', '!=', null)
            ->with(['lang', 'parent.lang'])
            ->get()->groupBy(function ($data) {
                return $data->parent->lang->name;
            });

        return response(['products' => $products, 'categories' => $categories], 200);
    }

    public function ser()
    {
//        $products = Product::first();
//        dd($products->fCategory);
        return view('front.ui.searchable');
    }
}
