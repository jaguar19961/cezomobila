<?php

namespace App\Http\Controllers;

use App\Models\Invoice;
use App\Models\Order;
use App\Models\Product;
use App\Models\ProductGallery;
use App\Models\ProductSpecification;
use App\Models\Specification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class CartController extends Controller
{
    public function getCart(Request $request)
    {
        $products = $request->all();
//        dd($products);
        $model = array();
        $total = 0;
        $delivery = 0;
        foreach ($products as $key => $product) {
            $prod = Product::with('color')->withOut('prodSpecs', 'galleries')->find($product['id']);
            $model[$key]['product'] = $prod;
            $model[$key]['quantity'] = $product['quantity'];
            $total += $product['quantity'] * $prod['price'];
        }
        $total += $delivery;
        return response(['model' => $model, 'total' => $total, 'delivery_price' => $delivery], 200);
    }

    public function storeCart(Request $request)
    {
//        dd($request->all());
        $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'country' => 'required',
            'city' => 'required',
            'address' => 'required',
            'delivery_price' => 'required',
            'total' => 'required',
            'delivery_method' => 'required',
            'payment_method' => 'required',
            'terms' => 'accepted',
            'products.*' => 'required|array',
        ]);

        $invoice = new Invoice();
        $invoice->invoice_id = $this->generateUuidNumber();
        $invoice->name = $request->name;
        $invoice->email = $request->email;
        $invoice->phone = $request->phone;
        $invoice->company = $request->company;
        $invoice->country = $request->country;
        $invoice->city = $request->city;
        $invoice->address = $request->address;
        $invoice->message = $request->message;
        $invoice->total = $request->total;
        $invoice->delivery_price = $request->delivery_price;
        $invoice->delivery_method = $request->delivery_method;
        $invoice->payment_method = $request->payment_method;
        $invoice->status = 1;
        $invoice->save();

        $prod = [];
        foreach ($request->products as $key => $product) {
//            dd($product['product']['id']);
            $order = new Order();
            $order->invoice_id = $invoice['id'];
            $order->product_id = $product['product']['id'];
            $order->sku = $product['product']['sku'];
            $order->quantity = $product['quantity'];
            $order->price = $product['product']['price'];
            $order->total = $product['quantity'] * $product['product']['price'];
            $order->save();

            $prod[] = [
                'sku' => $product['product']['locale_sku'],
                'image' => $product['product']['image'],
                'name' => $order['product']['name_ro'],
                'slug' => $order['product']['slug'],
                'quantity' => $order['quantity'],
                'price' => $order['price'],
                'total' => $order['total'],
            ];

        }

        $mailCollection = [
            'user_info' => $invoice,
            'req' => $prod,
            'total' => $invoice->total,
        ];
        $email = $request->email;
        Mail::send('front/confirmation', $mailCollection, function ($query) use ($email) {
            $query->from('support@cezomobila.md', 'Cezomobila');
            $query->to($email);
            $query->subject('Confirmare comanda');
        });

        $toEmail = 'cezomobila.md@gmail.com';
        Mail::send('front/confirmation', $mailCollection, function ($query) use ($toEmail) {
            $query->from('support@cezomobila.md', 'Cezomobila');
            $query->to($toEmail);
            $query->subject('Comanda noua');
        });

        return response(['status' => 200, 'order_nr' => $invoice['invoice_id']], 200);
    }

    public function generateUuidNumber()
    {
        $number = mt_rand(100000, 999999);
        if ($this->uuidNumberExists($number)) {
            return $this->generateUuidNumber();
        }
        return $number;
    }

    public function uuidNumberExists($number)
    {
        // query the database and return a boolean
        // for instance, it might look like this in Laravel
        return Invoice::where('invoice_id', $number)->exists();
    }
}
