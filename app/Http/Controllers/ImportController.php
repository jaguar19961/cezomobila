<?php

namespace App\Http\Controllers;

use App\Jobs\DownloadImages;
use App\Models\Brand;
use App\Models\Category;
use App\Models\ProductSpecification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;

class ImportController extends Controller
{
    protected $model;

    public function __construct(ProductSpecification $model)
    {
        $this->model = $model;
    }

    public function index()
    {
        $model = $this->model->where('excel', 1)->paginate(50);

        return view('auth.pages.import.index', compact('model'));
    }

    public function create()
    {
        $categories = Category::where('parent_id', '<>', 0)->get();
        $manufacturer = Brand::get();
        return view('auth.pages.import.create', compact('categories', 'manufacturer'));
    }

    public function store(Request $request)
    {
        $validator = $request->validate([
            'file' => 'required',
            'category_id' => 'required|numeric|min:0|not_in:0',
            'furnizor_id' => 'required|numeric|min:0|not_in:0',
        ]);
        $fileName = md5(now()) . '.' . $request->file('file')->getClientOriginalExtension();
        Storage::disk('public')->put('excel/' . $fileName, $request->file('file')->get());
        Artisan::call('backup:import', ['filename' => $fileName, 'category_id' => $request->get('category_id'), 'furnizor_id' => $request->get('furnizor_id')]);
        dispatch(new DownloadImages($request->get('category_id'), $request->get('furnizor_id')));
        return redirect()->back();
    }
}
