<?php

namespace App\Http\Middleware;

use App\Models\Restriction;
use Closure;
use Illuminate\Http\Request;

class IpMiddleware
{

    public $restrictIps = [];

    public function __construct(Restriction $restriction){
        $this->restrictIps = $restriction->where('accepted', 1)->pluck('ip')->toArray();
    }

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (in_array($request->ip(), $this->restrictIps)) {
            return $next($request);
        }
        Restriction::updateOrCreate([
            'ip' => $request->ip(),
        ]);
        return response()->view('errors.500', ['ip' => $request->ip()], 500);
    }
}
