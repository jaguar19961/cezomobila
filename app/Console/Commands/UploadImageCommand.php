<?php

namespace App\Console\Commands;

use App\Models\Shoes;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Spatie\Async\Pool;
use Image;

class UploadImageCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'backup:image';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    protected $data = array();

    public function __construct()
    {

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('Starting fetch products...');
        ini_set('memory_limit', '2048M');
        $this->info('set memory limit 2048M ');
        $this->info('Memory limit on server: ' . ini_get('memory_limit'));
        Shoes::chunk(10, function ($collections) {
            $collections->each(function ($item) {
                $it = Shoes::findOrFail($item->id);
                $it->image = $this->explodeImage($item->remote_images);
                $it->update();
                $this->info('Real memory usage:');
            });
            $this->info('End chunk...');
        });
        $this->info('Done Execution');
    }


    public function explodeImage($imageArray)
    {
        $pool = Pool::create();
        $this->data = array();
        if ($imageArray != null) {
            collect($imageArray)->map(function ($image) use ($pool) {
                $pool->add(function () use ($image) {
                    return $image;
                })->then(function ($image) {
                    $waterMarkUrl = '/images/logo-nav.png';
                    $file = Image::make($image)
                        ->orientate()
                        ->widen(600)->encode('jpg')
                        ->insert($waterMarkUrl, 'bottom-left');
                    $basePath = '/images/';
                    $relativePath = date('Y') . '/' . date('m') . '/' . date('d') . '/';
                    $fileName = sha1(microtime(true)) . '.jpg';
                    $fullPath = $basePath . $relativePath . $fileName;
                    Storage::disk('public')->put($fullPath, $file);
                    $this->data[] = $fullPath;
                })->catch(function (\Throwable $exception) {
                    // Handle exception
                });
            });
            $pool->concurrency(10);
            await($pool);
//            $pool->wait();
        }
        return $this->data;
    }
}
