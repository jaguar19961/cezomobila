<?php

namespace App\Console\Commands;

use App\Imports\ProductUpdateImport;
use App\Imports\ShoesImport;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

class ProductUpdateCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'backup:update {filename} {furnizor_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update price of products';

    /**
     * @var array|string|null
     */
    private $fileName;

    private $importPath = 'excel/';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $furnizor_id = $this->argument('furnizor_id');
        ini_set('memory_limit', '2048M');
        $this->fileName = $this->argument('filename');
        if (!$this->fileExists()) {
            $this->error('File does not exist !!!');
            die();
        }
        Excel::import(new ProductUpdateImport($furnizor_id), $this->getFile());
        $this->info('File successfully imported !');
        $this->deleteFile();

        return 0;
    }

    protected function fileExists(): bool
    {
        return Storage::disk('public')->exists($this->importPath . $this->fileName);
    }

    protected function getFile()
    {
        return Storage::disk('public')->path($this->importPath . $this->fileName);
    }

    protected function deleteFile()
    {
        Storage::disk('public')->delete($this->importPath . $this->fileName);
    }
}
