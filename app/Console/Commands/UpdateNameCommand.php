<?php

namespace App\Console\Commands;

use App\Models\Brand;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Console\Command;

class UpdateNameCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'products:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'update products name';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $products = Product::get();

        foreach ($products as $key => $product) {
            $product->complete_name_ro = self::generateName($product, 'ro');
            $product->complete_name_ru = self::generateName($product, 'ru');
            $product->save();

            $this->info('done' . self::generateName($product, 'ro') . '-' . $key);
        }
    }

    public function generateName($row, $lang)
    {
        if (!empty($row['antent_' . $lang])) {
            $category = $row['antent_' . $lang];
        } else {
            $categoryObj = Category::with(['transMany', 'lang'])->find($row->category_id);
            if ($categoryObj->transMany) {
                if (!empty($categoryObj->transMany->where('lang_id', $lang)->first()->description)) {
                    $category = $categoryObj->transMany->where('lang_id', $lang)->first()->description;
                } else {
                    if (!empty($categoryObj->transMany->where('lang_id', $lang)->first()->name)){
                        $category = $categoryObj->transMany->where('lang_id', $lang)->first()->name;
                    }else{
                        $category = 'NaN ';
                    }

                }
            } else {
                $category = '';
            }

        }
        $furnizorObj = Brand::find($row->furnizor_id);
        $brand = $furnizorObj['brand'];
        $model = $row['name_' . $lang];
        return $category . ' ' . $brand . ' ' . $model;
    }
}
