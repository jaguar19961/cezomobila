<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'categories';

    protected $model = 'App\\Models\\Category::class';

    protected $fillable = [
        'id',
        'slug',
        'parent_id',
        'image',
    ];

//    protected $with = ['lang', 'childs', 'attrib'];


    public function lang()
    {
        return $this->hasOne(Translation::class, 'article_id')
            ->where('model_name', $this->model)
            ->where('lang_id', app()->getLocale() ?? 'ro');
    }

    public function transMany()
    {
        return $this->hasMany(Translation::class, 'article_id')
            ->where('model_name', $this->model);
    }

    public function parent()
    {
        return $this->hasOne(self::class, 'id', 'parent_id');
    }

    public function childs()
    {
        return $this->hasMany(self::class, 'parent_id', 'id');
    }

    public function attrib(){
        return $this->hasMany(CategoryAtributePivot::class, 'category_id', 'id');
    }

    public function products()
    {
        return $this->hasMany(Product::class, 'category_id', 'id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'category_id', 'id');
    }
}
