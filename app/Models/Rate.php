<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{
    use HasFactory;

    protected $table = 'rates';

    protected $fillable = ['product_id', 'description', 'name', 'state'];

//    protected $with = ['product'];

    public function product(){
        return $this->hasOne(Product::class, 'id', 'product_id');
    }
}
