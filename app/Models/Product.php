<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'slug',
        'name_ro',
        'name_ru',
        'complete_name_ro',
        'complete_name_ru',
        'antet_ro',
        'antet_ru',
        'description_ro',
        'description_ru',
        'sku',
        'locale_sku',
        'category_id',
        'price',
        'price_discount',
        'price_buing',
        'offer_type_promotie',
        'offer_type_populare',
        'offer_type_new',
        'no_stock',
        'image',
        'galleries',
        'excel',
        'furnizor_id',
        'color_id',
        'similar_id'];

    protected $table = 'products';

    protected $model = 'App\\Models\\Product::class';

    protected $with = ['category.lang', 'rates', 'labels', 'productAttributes', 'color', 'images'];

    protected $appends = ['calc_rates', 'pluck_label', 'name', 'description'];

    public function getDescriptionAttribute()
    {
        return $this['description_' . app()->getLocale()];
    }

    public function color()
    {
        return $this->hasOne(Color::class, 'id', 'color_id');
    }

    public function combines()
    {
        return $this->hasOne(ProductCombine::class, 'id', 'similar_id');
    }

    public function productSpecifications()
    {
        return $this->hasMany(ProductSpecification::class, 'product_id', 'id');
    }

    public function productSpecificationsAsc()
    {
        return $this->hasMany(ProductSpecification::class, 'product_id', 'id')->orderBy('price', 'asc');
    }

    public function productAttributes()
    {
        return $this->hasMany(ProductSpecificationPivot::class, 'product_id', 'id');
    }

    public function category()
    {
        return $this->hasOne(Category::class, 'id', 'category_id');
    }

    public function fCategory()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id')
            ->with('lang');
    }

    public function rates()
    {
        return $this->hasMany(Rate::class, 'product_id', 'id');
    }

    public function furnizor()
    {
        return $this->hasOne(Brand::class, 'id', 'furnizor_id');
    }

    public function labels()
    {
        return $this->hasMany(ProductLabels::class, 'product_id', 'id');
    }

    public function GetPluckLabelAttribute()
    {
        return $this->labels()->pluck('label_id')->toArray();
    }

    public function GetCalcRatesAttribute()
    {
        $rates = $this->rates();
        $count = $rates->count();
        if ($count > 0) {
            $sum = $rates->sum('rate');
            $total = $sum / $count;
            return $total;
        } else {
            return 0;
        }


    }

    public function upsells()
    {
        return $this->hasMany(Upsell::class, '', 'id');
    }

    public function images()
    {
        return $this->hasMany(ProductGallery::class, 'product_id', 'id');
    }

    public function getNameAttribute()
    {
        $a = true;
        if ($a === true) {
            if (!empty($this['antent_' . app()->getLocale()])) {
                $category = $this['antent_' . app()->getLocale()];
            } else {
                if ($this->category->lang) {
                    if (!empty($this->category->lang->description)) {
                        $category = $this->category->lang->description;
                    } else {
                        $category = $this->category->lang->name;
                    }
                } else {
                    $category = '';
                }

            }
            $brand = $this->furnizor['brand'];
            $model = $this['name_' . app()->getLocale()];
            return $category . ' ' . $brand . ' ' . $model;
        } else {
            return $this['name_' . app()->getLocale()];
        }
    }
}
