<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SkuHistory extends Model
{
    use HasFactory;

    protected $table = 'sku_histories';

    protected $fillable = ['array', 'furnizor_id'];

    protected $casts = ['array' => 'array'];

    public function furnizor()
    {
        return $this->hasOne(Brand::class, 'id', 'furnizor_id');
    }
}
