<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Invoice extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'invoices';
    protected $model = 'App\\Models\\Invoice::class';
    protected $fillable = [
        'invoice_id',
        'name',
        'email',
        'phone',
        'company',
        'country',
        'city',
        'address',
        'message',
        'total',
        'delivery_price',
        'delivery_method',
        'payment_method',
        'status'];

    protected $with = ['local.lang'];

    public function orders()
    {
        return $this->hasMany(Order::class, 'invoice_id', 'id');
    }

    public function statusName()
    {
        return $this->hasOne(Status::class, 'id', 'status');
    }

    public function local()
    {
        return $this->hasOne(Local::class, 'id', 'city');
    }

    public function getPaymentMethodAttribute($item)
    {
        if ($item == 1) {
            return 'Achitare in numerar';
        }
        if ($item == 2) {
            return 'Achitare cu card';
        }
        if ($item == 3) {
            return 'Cumpar in credit';
        }
        return 0;
    }

    public function getDeliveryMethodAttribute($item)
    {
        if ($item == 1) {
            return 'Ridicare in oficiu';
        }
        if ($item == 2) {
            return 'Livrare domiciliu';
        }
        return 0;
    }
}
