<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CreditForm extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'credit_forms';
    protected $fillable = [
        'product_id',
        'quantity',
        'credit_id',
        'avans',
        'name',
        'phone',
    ];

    public function product()
    {
        return $this->hasOne(Product::class, 'id', 'product_id');
    }

    public function credit()
    {
        return $this->hasOne(Credit::class, 'id', 'credit_id');
    }
}
