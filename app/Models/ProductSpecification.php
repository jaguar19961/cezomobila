<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductSpecification extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = ['locale_sku', 'sku', 'product_id', 'price', 'price_discount', 'image', 'price_buing', 'galleries', 'furnizor_id', 'category_id', 'excel', 'name_ro', 'name_ru'];

    protected $table = 'product_specifications';

    protected $model = 'App\\Models\\ProductSpecification::class';
    protected $modelProduct = 'App\\Models\\Product::class';

    protected $casts = ['sub_specs' => 'array'];

    protected $with = ['prodSpecs', 'colorSpec', 'sizeSpec', 'galleries'];

    public function prodSpecs()
    {
        return $this->hasMany(ProductSpecificationPivot::class, 'spec_id', 'id');
    }

    public function colorSpec()
    {
        return $this->hasOne(ProductSpecificationPivot::class, 'spec_id', 'id')->where('attribute_id', 2);
    }

    public function sizeSpec()
    {
        return $this->hasOne(ProductSpecificationPivot::class, 'spec_id', 'id')->where('attribute_id', 112);
    }

    public function lang()
    {
        return $this->hasOne(Translation::class, 'article_id', 'product_id')
            ->where('model_name', $this->modelProduct)
            ->where('lang_id', app()->getLocale());
    }

    public function transMany()
    {
        return $this->hasMany(Translation::class, 'article_id', 'product_id')
            ->where('model_name', $this->modelProduct);
    }

    public function product()
    {
        return $this->hasOne(Product::class, 'id', 'product_id');
    }

    public function labels()
    {
        return $this->hasMany(ProductLabels::class, 'product_id', 'product_id');
    }

    public function galleries()
    {
        return $this->hasMany(ProductGallery::class, 'product_id', 'id');
    }

    public function category()
    {
        return $this->hasOne(Category::class, 'id', 'category_id');
    }
}
