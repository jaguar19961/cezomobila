<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductCombine extends Model
{
    use HasFactory;

    protected $table = 'product_combines';

    protected $fillable = ['sku_similar'];

    protected $with = ['products'];

    public function products()
    {
        return $this->hasMany(Product::class, 'similar_id', 'id');
    }
}
