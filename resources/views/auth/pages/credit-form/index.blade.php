@extends('auth.layouts.app')
@section('content')
    <!-- Dark table -->
    <div class="row">
        <div class="col">
            <div class="card bg-default shadow">
                <div class="card-header bg-transparent border-0 d-flex justify-content-between">
                    <h3 class="text-white mb-0">Credit</h3>
                    {{--                    <a href="{{route('admin.credit_form.create')}}" class="btn btn-sm btn-warning  mr-4 ">Create new</a>--}}
                </div>
                <div class="table-responsive">
                    <table class="table align-items-center table-dark table-flush">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col" class="sort" data-sort="name">Name</th>
                            <th scope="col" class="sort" data-sort="name">Phone</th>
                            <th scope="col" class="sort" data-sort="name">Product SKU</th>
                            <th scope="col" class="sort" data-sort="name">Product name</th>
                            <th scope="col" class="sort" data-sort="name">Credit name</th>
                            <th scope="col" class="sort" data-sort="name">Avans</th>
                            <th scope="col" class="sort" data-sort="name">Quantity</th>
                            <th scope="col" class="sort" data-sort="budget">Created</th>
                            <th scope="col" class="sort" data-sort="completion">Actions</th>
                        </tr>
                        </thead>
                        <tbody class="list">
                        @foreach($model as $item)
                            <tr>
                                <td class="budget">
                                    {{$item->name}}
                                </td>
                                <td class="budget">
                                    {{$item->phone}}
                                </td>
                                <td class="budget">
                                    <a target="_blank" href="/product/{{$item->product ? $item->product->slug : '0'}}">{{$item->product ? $item->product->name : 'Product deleted'}}</a>
                                </td>
                                <td class="budget">
                                    {{$item->product ? $item->product->sku : 'Product deleted'}}
                                </td>
                                <td class="budget">
                                    Luni:{{$item->credit->month}} Procent: {{$item->credit->interest_rate}}%
                                </td>
                                <td class="budget">
                                    {{$item->avans}} Lei
                                </td>
                                <td class="budget">
                                    {{$item->quantity}} seturi
                                </td>
                                <td class="budget">
                                    {{$item->created_at->format('d-m-Y H:i')}}
                                </td>

                                <td class="budget d-flex">
                                    <a href="{{route('admin.credit_form.destroy', $item->id)}}"
                                       class="btn btn-sm btn-danger  mr-4 ">Delete</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
