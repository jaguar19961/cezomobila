@extends('auth.layouts.app')
@section('content')
    <!-- Dark table -->
    <div class="row">
        <div class="col">
            <div class="card bg-default shadow">
                <div class="card-header bg-transparent border-0">
                    <h3 class="text-white mb-0">{{$model->name}}</h3>
                </div>
            </div>
            <div class="col-xl-12 order-xl-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">Details</h3>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="post" action="" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <input type="hidden" name="id" value="{{$model->id}}">
                            <h6 class="heading-small text-muted mb-4">Base information</h6>
                            <div class="pl-lg-4">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label"
                                                   for="input-username">Nume</label>
                                            <input type="text" id="input-username"
                                                   class="form-control" placeholder="Name"
                                                   name="name"
                                                   value="{{$model->name}}"
                                                   disabled="disabled">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label"
                                                   for="input-username">Email</label>
                                            <input type="text" id="input-username"
                                                   class="form-control" placeholder="Name"
                                                   name="name"
                                                   value="{{$model->email}}"
                                                   disabled="disabled">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label"
                                                   for="input-username">Phone</label>
                                            <input type="text" id="input-username"
                                                   class="form-control" placeholder="Name"
                                                   name="name"
                                                   value="{{$model->phone}}"
                                                   disabled="">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label"
                                                   for="input-username">Company</label>
                                            <input type="text" id="input-username"
                                                   class="form-control" placeholder="Name"
                                                   name="name"
                                                   value="{{$model->company}}"
                                                   disabled="">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label"
                                                   for="input-username">Country</label>
                                            <input type="text" id="input-username"
                                                   class="form-control" placeholder="Name"
                                                   name="name"
                                                   value="{{$model->country == 1 ? 'Moldova' : ''}}"
                                                   disabled="">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label"
                                                   for="input-username">City</label>
                                            <input type="text" id="input-username"
                                                   class="form-control" placeholder="Name"
                                                   name="name"
                                                   value="{{$model->local->lang->name}}"
                                                   disabled="">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label"
                                                   for="input-username">Adress</label>
                                            <input type="text" id="input-username"
                                                   class="form-control" placeholder="Name"
                                                   name="name"
                                                   value="{{$model->address}}"
                                                   disabled="">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label"
                                                   for="input-username">Message</label>
                                            <input type="text" id="input-username"
                                                   class="form-control" placeholder="Name"
                                                   name="name"
                                                   value="{{$model->message}}"
                                                   disabled="">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label"
                                                   for="input-username">Delivery</label>
                                            <input type="text" id="input-username"
                                                   class="form-control" placeholder="Name"
                                                   name="name"
                                                   value="{{$model->delivery_method}}"
                                                   disabled="">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label"
                                                   for="input-username">Payment</label>
                                            <input type="text" id="input-username"
                                                   class="form-control" placeholder="Name"
                                                   name="name"
                                                   value="{{$model->payment_method}}"
                                                   disabled="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr class="my-4"/>
                            {{--                            <button type="submit" class="btn btn-success">Save</button>--}}
                        </form>
                        <br>
                        <div class="table-responsive">
                            <table class="table align-items-center table-dark table-flush">
                                <thead class="thead-dark">
                                <tr>
                                    <th scope="col" class="sort" data-sort="name">SKU</th>
                                    <th scope="col" class="sort" data-sort="name">Product Name</th>
                                    <th scope="col" class="sort" data-sort="name">Color</th>
                                    <th scope="col" class="sort" data-sort="name">Quantity</th>
                                    <th scope="col" class="sort" data-sort="name">Price</th>
                                    <th scope="col" class="sort" data-sort="name">Total</th>
                                </tr>
                                </thead>
                                <tbody class="list">
{{--                                                                @dd($model->orders)--}}
                                @foreach($model->orders as $item)
                                    <tr>
                                        <td class="budget">
                                            {{$item->sku}}
                                        </td>
                                        <td class="budget">
                                            <a target="_blank" href="/product/{{$item->product->slug}}">{{$item->product->name}}</a>
                                        </td>
                                        <td class="budget">
                                            {{$item->product->color ? $item->product->color->lang->name : 'No color'}}
                                        </td>
                                        <td class="budget">
                                            {{$item->quantity}}
                                        </td>
                                        <td class="budget">
                                            {{$item->price}}
                                        </td>
                                        <td class="budget">
                                            {{$item->total}}
                                        </td>
                                    </tr>
                                @endforeach
                                <tr>
                                    <td class="budget">

                                    </td>
                                    <td class="budget">

                                    </td>
                                    <td class="budget">

                                    </td>
                                    <td class="budget">

                                    </td>
                                    <td class="budget">
                                        Subtotal:
                                    </td>
                                    <td class="budget">
                                        {{$model->total - $model->delivery_price}}
                                    </td>
                                </tr>
                                <tr>
                                    <td class="budget">

                                    </td>
                                    <td class="budget">

                                    </td>
                                    <td class="budget">

                                    </td>
                                    <td class="budget">

                                    </td>
                                    <td class="budget">
                                        Pret livrare:
                                    </td>
                                    <td class="budget">
                                        {{$model->delivery_price}}
                                    </td>
                                </tr>
                                <tr>
                                    <td class="budget">

                                    </td>
                                    <td class="budget">

                                    </td>
                                    <td class="budget">

                                    </td>
                                    <td class="budget">

                                    </td>
                                    <td class="budget">
                                        Total:
                                    </td>
                                    <td class="budget">
                                        {{$model->total}}
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
