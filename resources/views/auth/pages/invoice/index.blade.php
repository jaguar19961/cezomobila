@extends('auth.layouts.app')
@section('content')
    <!-- Dark table -->
    <div class="row">
        <div class="col">
            <div class="card bg-default shadow">
                <div class="card-header bg-transparent border-0 d-flex justify-content-between">
                    <h3 class="text-white mb-0">Orders</h3>
                    {{--                    <a href="{{route('admin.credit.create')}}" class="btn btn-sm btn-warning  mr-4 ">Create new</a>--}}
                </div>
                <div class="table-responsive">
                    <table class="table align-items-center table-dark table-flush">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col" class="sort" data-sort="name">Name</th>
                            <th scope="col" class="sort" data-sort="name">Email</th>
                            <th scope="col" class="sort" data-sort="name">Telefon</th>
                            <th scope="col" class="sort" data-sort="name">Adresa</th>
                            <th scope="col" class="sort" data-sort="name">Pret livrare</th>
                            <th scope="col" class="sort" data-sort="name">Livrare</th>
                            <th scope="col" class="sort" data-sort="name">Plata</th>
                            <th scope="col" class="sort" data-sort="status">Status</th>
                            <th scope="col" class="sort" data-sort="budget">Created</th>
                            <th scope="col" class="sort" data-sort="completion">Actions</th>
                        </tr>
                        </thead>
                        <tbody class="list">
                        @foreach($model as $item)
                            <tr>
                                <td class="budget">
                                    {{$item->name}}
                                </td>
                                <td class="budget">
                                    {{$item->email}}
                                </td>
                                <td class="budget">
                                    {{$item->phone}}
                                </td>
                                <td class="budget">
                                    {{$item->country == 1 ? 'Moldova' : ''}}, {{$item->local ? $item->local->lang->name : ''}}, {{$item->address}}
                                </td>
                                <td class="budget">
                                    {{$item->delivery_price}} Lei
                                </td>
                                <td class="budget">
                                    {{$item->delivery_method}}
                                </td>
                                <td class="budget">
                                    {{$item->payment_method}}
                                </td>
                                <td class="budget">
                                    {{$item->statusName->name}}
                                </td>
                                <td class="budget">
                                    {{$item->created_at->format('d-m-Y H:i')}}
                                </td>

                                <td class="budget">
                                    <a href="{{route('admin.invoice.show', $item->id)}}"
                                       class="btn btn-sm btn-info  mr-4 ">Details</a>
                                    <a href="{{route('admin.invoice.destroy', $item->id)}}"
                                       class="btn btn-sm btn-danger  mr-4 ">Delete</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
