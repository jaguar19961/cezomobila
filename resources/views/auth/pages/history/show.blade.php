@extends('auth.layouts.app')
@section('content')
    <!-- Dark table -->
    <div class="row">
        <div class="col">
            <div class="card bg-default shadow">
                <div class="card-header bg-transparent border-0">
                    <h3 class="text-white mb-0">{{$model->name}}</h3>
                </div>
            </div>
            <div class="col-xl-12 order-xl-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">Edit - {{$model->name}} </h3>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{route('admin.brand.update')}}" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <input type="hidden" value="{{$model->id}}" name="id">
                            <h6 class="heading-small text-muted mb-4">Information</h6>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-username">Name</label>
                                        <input type="text" id="input-username" class="form-control" placeholder="Name" name="name" value="{{$model->name}}" required>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-username">Brand</label>
                                        <input type="text" id="input-username" class="form-control" placeholder="Brand" name="brand" value="{{$model->brand}}" required>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-username">Phone</label>
                                        <input type="text" id="input-username" class="form-control" placeholder="Phone" name="phone" value="{{$model->phone}}" required>
                                    </div>
                                </div>
{{--                                <div class="col-lg-6">--}}
{{--                                    <div class="form-group">--}}
{{--                                        <label class="form-control-label" for="input-last-name">Image</label>--}}
{{--                                        <div class="custom-file">--}}
{{--                                            <input type="file" name="image" class="custom-file-input"--}}
{{--                                                   id="customFileLang" lang="en">--}}
{{--                                            <label class="custom-file-label" for="customFileLang">Select file</label>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
                            </div>
                            <hr class="my-4"/>
                            <button type="submit" class="btn btn-success">Save</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
