@extends('auth.layouts.app')
@section('content')
    <!-- Dark table -->
    <div class="row">
        <div class="col">
            <div class="card bg-default shadow">
                <div class="card-header bg-transparent border-0 d-flex justify-content-between">
                    <h3 class="text-white mb-0">Sku history</h3>
                    {{--                    <a href="{{route('admin.brand.create')}}" class="btn btn-sm btn-warning  mr-4 ">Create new</a>--}}
                </div>
                <div class="table-responsive">
                    <table class="table align-items-center table-dark table-flush">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col" class="sort" data-sort="name">Status</th>
                            <th scope="col" class="sort" data-sort="name">Sku</th>
                            <th scope="col" class="sort" data-sort="name">Furnizor</th>
                            <th scope="col" class="sort" data-sort="budget">Created</th>
                        </tr>
                        </thead>
                        <tbody class="list">
                        @foreach($model as $it)
                            @foreach($it->array as $item)
                                <tr>
                                    <td class="budget">
                                        <span class="badge badge-warning">Not found</span>
                                    </td>
                                    <td class="budget">
                                        {{$item}}
                                    </td>
                                    <td class="budget">
                                        {{$it->furnizor->name}}
                                    </td>
                                    <td class="budget">
                                        {{$it->created_at->format('d-m-Y')}}
                                    </td>
                                </tr>
                            @endforeach
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
