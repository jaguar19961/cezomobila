@extends('auth.layouts.app')
@section('content')
    <!-- Dark table -->
    <div class="row">
        <div class="col">
            <div class="card bg-default shadow">
                <div class="card-header bg-transparent border-0 d-flex justify-content-between">
                    <h3 class="text-white mb-0">Specification <strong class="text-underline">{{$parent_name->lang->name}}</strong></h3>
                    <div>
                        <a href="{{route('admin.specification.show', $parent_name->id)}}" class="btn btn-sm btn-info  mr-4 ">Edit this specification</a>
                        <a href="{{route('admin.specification.create')}}" class="btn btn-sm btn-warning  mr-4 ">Create new</a>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table align-items-center table-dark table-flush">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col" class="sort" data-sort="name">Name</th>
                            <th scope="col" class="sort" data-sort="budget">Parent</th>
                            <th scope="col" class="sort" data-sort="budget">Created</th>
                            <th scope="col" class="sort" data-sort="completion">Actions</th>
                        </tr>
                        </thead>
                        <tbody class="list">
                        @foreach($model as $item)
                        <tr>
                            <td class="budget">
	                            {{$item->lang->name}}
                            </td>
                            <td class="budget">
                                {{$item->parent ? $item->parent->lang->name : 'Not have parent'}}
                            </td>
                            <td class="budget">
                                {{$item->created_at->format('d-m-Y')}}
                            </td>

                            <td class="budget">
                                <a href="{{route('admin.specification.show', $item->id)}}" class="btn btn-sm btn-info  mr-4 ">Edit</a>
                                <a href="{{route('admin.specification.destroy', $item->id)}}" class="btn btn-sm btn-danger  mr-4 ">Delete</a>
                            </td>
                        </tr>
                            @foreach($item->childs as $child)
                                <tr style="background-color: #0b1c39">
                                    <td class="budget">
                                        <i class="fa fa-arrow-right"></i>
                                        {{$child->lang->name}}
                                    </td>
                                    <td class="budget">

                                    </td>
                                    <td class="budget">
                                        {{$child->created_at->format('d-m-Y')}}
                                    </td>

                                    <td class="budget">
                                        <a href="{{route('admin.specification.show', $child->id)}}" class="btn btn-sm btn-info  mr-4 ">Edit</a>
                                        <a href="{{route('admin.specification.destroy', $child->id)}}" class="btn btn-sm btn-danger  mr-4 ">Delete</a>
                                    </td>
                                </tr>
                            @endforeach
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
