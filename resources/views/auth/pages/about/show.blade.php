@extends('auth.layouts.app')
@section('content')
    <!-- Dark table -->
    <div class="row">
        <div class="col">
            <div class="card bg-default shadow">
                <div class="card-header bg-transparent border-0">
                    <h3 class="text-white mb-0">{{$model->name}}</h3>
                </div>
            </div>
            <div class="col-xl-12 order-xl-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">Create new post</h3>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{route('admin.about.update')}}" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <input type="hidden" name="id" value="{{$model->id}}">
                            <h6 class="heading-small text-muted mb-4">User information</h6>
                            <div class="pl-lg-4">
                                <div class="accordion" id="accordionExample">
                                    @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                                        <div class="card">
                                            <div class="card-header" id="heading{{$localeCode}}">
                                                <h2 class="mb-0">
                                                    <button class="btn btn-link btn-block text-left" type="button"
                                                            data-toggle="collapse"
                                                            data-target="#collapse{{$localeCode}}" aria-expanded="true"
                                                            aria-controls="collapse{{$localeCode}}">
                                                        Text spre completare - [{{$localeCode}}]
                                                    </button>
                                                </h2>
                                            </div>
                                            <div id="collapse{{$localeCode}}"
                                                 class="collapse {{$localeCode === Lang::locale() ? 'show' : ''}}"
                                                 aria-labelledby="heading{{$localeCode}}"
                                                 data-parent="#accordionExample">
                                                <div class="card-body">
                                                    <div class="pl-lg-4">
                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <div class="form-group">
                                                                    <label class="form-control-label">Text 1 [{{$localeCode}}]</label>
                                                                    <textarea required rows="4" name="name[{{$localeCode}}]"
                                                                              class="form-control"
                                                                              placeholder="A few words about you ...">{{$model->transMany->where('lang_id', $localeCode)->first()->name}}</textarea>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-12">
                                                                <div class="form-group">
                                                                    <label class="form-control-label">Text 2 [{{$localeCode}}]</label>
                                                                    <textarea required rows="4" name="meta_name[{{$localeCode}}]"
                                                                              class="form-control"
                                                                              placeholder="A few words about you ...">{{$model->transMany->where('lang_id', $localeCode)->first()->meta_name}}</textarea>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-12">
                                                                <div class="form-group">
                                                                    <label class="form-control-label">Text 3 [{{$localeCode}}]</label>
                                                                    <textarea required rows="4" name="meta_description[{{$localeCode}}]"
                                                                              class="form-control"
                                                                              placeholder="A few words about you ...">{{$model->transMany->where('lang_id', $localeCode)->first()->meta_description}}</textarea>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-12">
                                                                <div class="form-group">
                                                                    <label class="form-control-label">Text 4 [{{$localeCode}}]</label>
                                                                    <textarea required rows="4" name="delivery[{{$localeCode}}]"
                                                                              class="form-control"
                                                                              placeholder="A few words about you ...">{{$model->transMany->where('lang_id', $localeCode)->first()->delivery}}</textarea>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-12">
                                                                <div class="form-group">
                                                                    <label class="form-control-label">Text 5 [{{$localeCode}}]</label>
                                                                    <textarea required rows="4" name="description[{{$localeCode}}]"
                                                                              class="form-control"
                                                                              placeholder="A few words about you ...">{{$model->transMany->where('lang_id', $localeCode)->first()->description}}</textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="input-last-name">Image</label>
                                            <div class="custom-file">
                                                <input type="file" name="image" class="custom-file-input"
                                                       id="customFileLang" lang="en">
                                                <label class="custom-file-label" for="customFileLang">Select
                                                    file</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <img style="width: 100px;" src="{{asset($model->image_1)}}" alt="">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="input-last-name">Image</label>
                                            <div class="custom-file">
                                                <input type="file" name="image1" class="custom-file-input"
                                                       id="customFileLang" lang="en">
                                                <label class="custom-file-label" for="customFileLang">Select
                                                    file</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <img style="width: 100px;" src="{{asset($model->image_2)}}" alt="">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="input-last-name">Image 3</label>
                                            <div class="custom-file">
                                                <input type="file" name="image2" class="custom-file-input"
                                                       id="customFileLang" lang="en">
                                                <label class="custom-file-label" for="customFileLang">Select
                                                    file</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <img style="width: 100px;" src="{{asset($model->image_3)}}" alt="">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="input-last-name">Image 4</label>
                                            <div class="custom-file">
                                                <input type="file" name="image3" class="custom-file-input"
                                                       id="customFileLang" lang="en">
                                                <label class="custom-file-label" for="customFileLang">Select
                                                    file</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <img style="width: 100px;" src="{{asset($model->image_4)}}" alt="">
                                    </div>
                                </div>
                            </div>
                            <hr class="my-4"/>
                            <button type="submit" class="btn btn-success">Save</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
