@extends('auth.layouts.app')
@section('content')
    <!-- Dark table -->
    <div class="row">
        <div class="col">
            <div class="card bg-default shadow">
                <div class="card-header bg-transparent border-0 d-flex justify-content-between">
                    <h3 class="text-white mb-0">Product import</h3>
                   <div>
                       <a href="{{route('admin.import.create')}}" class="btn btn-sm btn-warning  mr-4 ">Create new</a>
                       <a href="{{route('admin.import.show')}}" class="btn btn-sm btn-primary  mr-4 ">Update old</a>
                   </div>
                </div>
                <div class="table-responsive">
                    <table class="table align-items-center table-dark table-flush">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col" class="sort" data-sort="name">Name</th>
                            <th scope="col" class="sort" data-sort="name">SKU</th>
                            <th scope="col" class="sort" data-sort="budget">Price</th>
                            <th scope="col" class="sort" data-sort="budget">Price discount</th>
                            <th scope="col" class="sort" data-sort="budget">Price buy</th>
                            <th scope="col" class="sort" data-sort="budget">Category</th>
                            <th scope="col" class="sort" data-sort="completion">Actions</th>
                        </tr>
                        </thead>
                        <tbody class="list">
                        @foreach($model as $item)
                            <tr>
                                <td class="budget">
                                    {{$item->slug}}
                                </td>
                                <td class="budget">
                                    # {{$item->locale_sku}}
                                </td>
                                <td class="budget">
                                    {{$item->price}}
                                </td>
                                <td class="budget">
                                    {{$item->price_discount}}
                                </td>
                                <td class="budget">
                                    {{$item->price_buing}}
                                </td>
                                <td class="budget">
                                    {{$item->category_id}}
                                </td>

                                <td class="budget">

                                </td>

                                {{--                            <td class="budget">--}}
                                {{--                                <a href="{{route('admin.blog.show', $item->id)}}" class="btn btn-sm btn-info  mr-4 ">Edit</a>--}}
                                {{--                                <a href="{{route('admin.blog.destroy', $item->id)}}" class="btn btn-sm btn-danger  mr-4 ">Delete</a>--}}
                                {{--                            </td>--}}
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="pagin_old col-12 col-md-12 m-auto mb-5 pb-3 pt-2">
                    {{$model->render()}}
                </div>
            </div>
        </div>
    </div>
@endsection
