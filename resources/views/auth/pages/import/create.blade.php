@extends('auth.layouts.app')
@section('content')
    <!-- Dark table -->
    <div class="row">
        <div class="col">
            <div class="card bg-default shadow">
                <div class="card-header bg-transparent border-0">
                    <h3 class="text-white mb-0">Import Create</h3>
                </div>
            </div>
            <div class="col-xl-12 order-xl-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">Create new page</h3>
                            </div>
                        </div>
                    </div>
                    @if($errors->any())
                        {!! implode('', $errors->all('<div>:message</div>')) !!}
                    @endif
                    <div class="card-body">
                        <form method="post" action="{{route('admin.import.store')}}" enctype="multipart/form-data">
                            {{csrf_field()}}
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="input-last-name">Excel file</label>
                                            <div class="custom-file">
                                                <input type="file" name="file" class="custom-file-input"
                                                       id="customFileLang" lang="en" required>
                                                <label class="custom-file-label" for="customFileLang">Select
                                                    file</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="exampleFormControlSelect1">Category</label>
                                            <select class="form-control" name="category_id" id="exampleFormControlSelect1">
                                                <option value="0" disabled selected>Select Category</option>
                                                @foreach($categories as $item)
                                                    <option value="{{$item->id}}">{{$item->lang['name']}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="exampleFormControlSelect1">Manufacture</label>
                                            <select class="form-control" name="furnizor_id" id="exampleFormControlSelect1">
                                                <option value="0" disabled selected>Select manufacture</option>
                                                @foreach($manufacturer as $item)
                                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            <hr class="my-4"/>
                            <button type="submit" class="btn btn-success">Save</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
