@extends('auth.layouts.app')
@section('content')
    <!-- Dark table -->
    <div class="row">
        <div class="col">
            <form role="form" action="{{route('admin.products.search')}}" method="POST"
                  class="navbar-search navbar-search-light form-inline mr-sm-3 mb-5" id="navbar-search-main">
                @csrf
                <div class="form-group mb-0">
                    <div class="input-group input-group-alternative input-group-merge">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-search"></i></span>
                        </div>
                        <input class="form-control" placeholder="Search" type="text" name="search"
                               value="{{$data['search']}}">
                    </div>
                </div>
                <div class="form-group mb-0">
                    <div class="input-group input-group-alternative input-group-merge">
                        <select name="category_id" class="form-control" id="exampleFormControlSelect1">
                            <option value="0">All</option>
                            @foreach($categories as $category)
                                <option @if($category->id == $data['category_id']) selected
                                        @endif value="{{$category->id}}">{{$category->lang->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group mb-0">
                    <div class="input-group input-group-alternative input-group-merge">
                        <select name="furnizor_id" class="form-control" id="exampleFormControlSelect1">
                            <option value="0">All</option>
                            @foreach($furnizors as $furnizor)
                                <option @if($furnizor->id == $data['category_id']) selected
                                        @endif value="{{$furnizor->id}}">{{$furnizor->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <button type="submit" class="btn btn-success ml-4 mr-4">Search!</button>
                <a href="{{route('admin.products')}}" class="btn btn-warning ml-4 mr-4 ">Reset filter</a>
            </form>

            <div class="card bg-default shadow">
                <div class="card-header bg-transparent border-0 d-flex justify-content-between">
                    <h3 class="text-white mb-0">Products</h3>
                    <a href="{{route('admin.products.create')}}" class="btn btn-sm btn-warning  mr-4 ">Create new</a>
                </div>
                <div class="table-responsive">
                    <table class="table align-items-center table-dark table-flush">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col" class="sort" data-sort="name">Reseted</th>
                            <th scope="col" class="sort" data-sort="name">Name</th>
                            <th scope="col" class="sort" data-sort="name">SKU local</th>
                            <th scope="col" class="sort" data-sort="name">SKU furnizor</th>
                            <th scope="col" class="sort" data-sort="name">Img</th>
                            <th scope="col" class="sort" data-sort="name">Similar ID</th>
                            <th scope="col" class="sort" data-sort="name">Category</th>
                            <th scope="col" class="sort" data-sort="name">Furnizor</th>
                            <th scope="col" class="sort" data-sort="name">Furnizor phone</th>
                            <th scope="col" class="sort" data-sort="budget">Created</th>
                            <th scope="col" class="sort" data-sort="completion">Actions</th>
                        </tr>
                        </thead>
                        <tbody class="list">
                        @foreach($model as $item)
                            <tr>
                                <td class="budget">
                                    @if(!empty($item->reseted_at))
                                        <span class="badge badge-success">In stock</span>
                                    @else
                                        <span class="badge badge-warning">Not found</span>
                                    @endif
                                </td>
                                <td class="budget">
                                    <a target="_blank" href="{{url('/product/'.$item->slug)}}">{{$item->name_ro}}</a>
                                </td>
                                <td class="budget">
                                    {{$item->locale_sku}}
                                </td>
                                <td class="budget">
                                    {{$item->sku}}
                                </td>
                                <td class="budget text-center">
                                    <img src="{{$item->image}}" width="30px" height="30px" alt="">
                                </td>
                                <td class="budget text-center">
                                    {{$item->similar_id}}
                                </td>
                                <td class="budget">
                                    {{$item->category ? $item->category->lang->name : 'Nu este definit'}}
                                </td>
                                <td class="budget">
                                    {{$item->furnizor ? $item->furnizor->name : 'Nu este furnizor'}}
                                </td>
                                <td class="budget">
                                    {{$item->furnizor ? $item->furnizor->phone : 'Nu este furnizor'}}
                                </td>

                                <td class="budget">
                                    {{$item->created_at->format('d-m-Y')}}
                                </td>

                                <td class="budget">
                                    <a data-toggle="modal" data-target="#promotie{{$item->id}}" href="?"
                                       class="btn btn-sm btn-info  mr-4 ">Promotie</a>
                                    <a data-toggle="modal" data-target="#exampleModal{{$item->id}}" href="?"
                                       class="btn btn-sm btn-info  mr-4 ">Detail</a>
                                    <a href="{{route('admin.products.show', $item->id)}}"
                                       class="btn btn-sm btn-info  mr-4 ">Edit</a>
                                    <a href="{{route('admin.products.destroy', $item->id)}}"
                                       class="btn btn-sm btn-danger  mr-4 ">Delete</a>
                                </td>
                                <!-- Modal -->
                                <div class="modal fade" id="exampleModal{{$item->id}}" tabindex="-1"
                                     aria-labelledby="exampleModalLabel{{$item->id}}" aria-hidden="true">
                                    <div class="modal-dialog modal-xl">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel{{$item->id}}">Product
                                                    info</h5>
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <admin-combine-products
                                                            :product_id="{{json_encode($item->id)}}"></admin-combine-products>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

{{--                                //promotie--}}
                                <div class="modal fade" id="promotie{{$item->id}}" tabindex="-1"
                                     aria-labelledby="promotieLabel{{$item->id}}" aria-hidden="true">
                                    <div class="modal-dialog modal-xl">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="promotieLabel{{$item->id}}">Product
                                                    info</h5>
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <admin-upsells-module :product_id="{{json_encode($item->id)}}" page_type="product"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </tr>
                        @endforeach
                        @if($model->count() === 0)
                            <tr>
                                <td><span>Nu sa gasit nici un rezultat!</span></td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
                @if($model instanceof \Illuminate\Pagination\LengthAwarePaginator)
                    <div class="pagin_old col-12 col-md-12 m-auto mb-5 pb-3 pt-2 pagin_old">
                        {{$model->render()}}
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
