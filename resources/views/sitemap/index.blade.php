<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <url>
        <loc>{{env('APP_URL')}}</loc>
        <lastmod>{{ \Carbon\Carbon::now()->tz('UTC')->toAtomString() }}</lastmod>
        <changefreq>weekly</changefreq>
        <priority>0.6</priority>
    </url>
    <url>
        <loc>{{env('APP_URL')}}/info/comanda-si-livrare</loc>
        <lastmod>{{ \Carbon\Carbon::now()->tz('UTC')->toAtomString() }}</lastmod>
        <changefreq>weekly</changefreq>
        <priority>0.6</priority>
    </url>
    <url>
        <loc>{{env('APP_URL')}}/contacts</loc>
        <lastmod>{{ \Carbon\Carbon::now()->tz('UTC')->toAtomString() }}</lastmod>
        <changefreq>weekly</changefreq>
        <priority>0.6</priority>
    </url>
    <url>
        <loc>{{env('APP_URL')}}/cart</loc>
        <lastmod>{{ \Carbon\Carbon::now()->tz('UTC')->toAtomString() }}</lastmod>
        <changefreq>weekly</changefreq>
        <priority>0.6</priority>
    </url>
    @foreach ($products as $product)
        <url>
            <loc>{{env('APP_URL')}}/product/{{ $product->slug }}</loc>
            <lastmod>{{ $product->created_at->tz('UTC')->toAtomString() }}</lastmod>
            <changefreq>weekly</changefreq>
            <priority>0.6</priority>
        </url>
    @endforeach
{{--    @foreach ($categories as $category)--}}
{{--        <url>--}}
{{--            <loc>{{env('APP_URL')}}/catalog/{{ $category->slug }}</loc>--}}
{{--            <lastmod>{{ $category->created_at->tz('UTC')->toAtomString() }}</lastmod>--}}
{{--            <changefreq>weekly</changefreq>--}}
{{--            <priority>0.6</priority>--}}
{{--        </url>--}}
{{--    @endforeach--}}
            @foreach ($categories as $category)
        <url>
            <loc>{{env('APP_URL')}}/catalog?category_id={{ $category->id }}</loc>
            <lastmod>{{ $category->created_at->tz('UTC')->toAtomString() }}</lastmod>
            <changefreq>weekly</changefreq>
            <priority>0.6</priority>
        </url>
    @endforeach
</urlset>
