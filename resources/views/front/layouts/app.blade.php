<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property="og:type" content="product">

    {!! Meta::tag('robots') !!}

    {!! Meta::tag('site_name', 'Cezomobila.md - mobila pe placul tuturor.') !!}
    {!! Meta::tag('url', Request::url()); !!}
    {!! Meta::tag('locale', app()->getLocale()) !!}

    {!! Meta::tag('title') !!}
    {!! Meta::tag('description') !!}

    {!! Meta::tag('canonical', url()->full()) !!}
    {!! Meta::tag('image', asset('/img/cezo_logo.svg')) !!}

    @if(!empty($product))
        <meta property="product:brand" content="CEZOMOBILA">
        <meta property="product:availability" content="{{$product->no_stock === 0 ? 'in stock' : 'out of stock' }}">
        <meta property="product:condition" content="new">
        <meta property="product:price:amount" content="{{$product->no_stock === 0 ? $product->price : 0 }}">
        <meta property="product:price:currency" content="MDL">
{{--        <meta property="product:override" content="{{app()->getLocale()}}">--}}
        <meta property="product:retailer_item_id" content="{{$product->locale_sku}}">
        <meta property="product:item_group_id" content="{{$product->combines ? $product->combines->sku_similar : $product->complete_name_ro}}">
    @endif


    <link rel="apple-touch-icon" sizes="57x57" href="/images/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/images/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/images/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/images/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/images/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/images/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/images/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/images/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/images/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="/images/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/images/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/images/favicon/favicon-16x16.png">
    <link rel="manifest" href="/images/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/images/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@400;600;700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/all.min.css') }}">

    <!-- Styles -->
    <link href="{{ mix('css/main.css') }}" rel="stylesheet">
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-MV7DK3Q');</script>
    <!-- End Google Tag Manager -->
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MV7DK3Q"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div id="app" v-cloak>
    @include('front.ui.header')
    <main class="@if(Route::currentRouteName() != 'index') py-4 @endif">
        @yield('content')
    </main>
    @include('front.ui.footer')
</div>

@if($localBusiness ?? false)
    {!! $localBusiness->toScript() !!}
@endif

@if(Route::currentRouteName() === 'confirm')
{{--    //multumire--}}
    <script>
        gtag('event', 'conversion', {
            'send_to': 'AW-572604593/21hYCKGE6sUCELGBhZEC',
            'value': 1.0,
            'currency': 'MDL',
            'transaction_id': {{$invoice_id}}
        });
    </script>
@endif
<script>
    window.lang = @json(app()->getLocale())
</script>

<!-- Facebook Pixel Code -->
<script>
    !function (f, b, e, v, n, t, s) {
        if (f.fbq) return;
        n = f.fbq = function () {
            n.callMethod ?
                n.callMethod.apply(n, arguments) : n.queue.push(arguments)
        };
        if (!f._fbq) f._fbq = n;
        n.push = n;
        n.loaded = !0;
        n.version = '2.0';
        n.queue = [];
        t = b.createElement(e);
        t.async = !0;
        t.src = v;
        s = b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t, s)
    }(window, document, 'script',
        'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '722542068681261');
    fbq('track', 'PageView');
    @if(!empty($product))
    fbq('track', 'ViewContent', {
        content_type: 'product',
        content_ids: ["{{$product->locale_sku}}"],
        value: {{$product->price}},
        override: {{app()->getLocale()}},
        item_group_id: {{$product->combines ? $product->combines->sku_similar : $product->complete_name_ro}},
        currency: 'MDL',
    });
    @endif
</script>
<noscript><img height="1" width="1" style="display:none"
               src="https://www.facebook.com/tr?id=722542068681261&ev=PageView&noscript=1"
    /></noscript>
<!-- End Facebook Pixel Code -->
<div class="d-none">{!! $script->name !!}</div>
<!-- Scripts -->
<script src="{{ mix('js/app.js') }}" defer></script>
</body>
</html>
