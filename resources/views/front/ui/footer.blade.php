<footer>
    <section class="instagram-feed">
        <div class="container-xl">
            <div class="row">
                <div class="col-md-4">
                    <div class="instagram_description">
                        <div class="icon"><i class="fab fa-instagram"></i></div>
                        <p>{!! instatext() ? instatext()->lang->name : 'No translation' !!} </p>
                        <a href="https://www.instagram.com/cezomobila.md/">{!! instatext() ? instatext()->lang->meta_name : 'No translation' !!}
                            <span><i class="fa fa-chevron-right"></i></span></a>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="row mt-4">
                        @foreach(instas() as $item)
                            <div class="col-4 col-sm-3 col-lg">
                                {{--                            <img src="{{asset($item->image)}}" alt="instagram" class="img-fluid">--}}
                                <app-image :lazy-src="{{json_encode(asset($item->image))}}"
                                           :lazy-srcset="{{json_encode(asset($item->image))}}"
                                           alt="instagram"
                                           class_list="img-fluid"/>
                            </div>
                        @endforeach
                        {{--                        <div class="col-4 col-sm-3 col-lg">--}}
                        {{--                            <img src="{{asset('/images/insta/2.jpeg')}}" alt="instagram" class="img-fluid">--}}
                        {{--                        </div>--}}
                        {{--                        <div class="col-4 col-sm-3 col-lg">--}}
                        {{--                            <img src="{{asset('/images/insta/3.jpeg')}}" alt="instagram" class="img-fluid">--}}
                        {{--                        </div>--}}
                        {{--                        <div class="col-4 col-sm-3 col-lg d-none d-lg-block">--}}
                        {{--                            <img src="{{asset('/images/insta/4.jpeg')}}" alt="instagram" class="img-fluid">--}}
                        {{--                        </div>--}}
                        {{--                        <div class="col-4 col-sm-3 col-lg d-none d-lg-block">--}}
                        {{--                            <img src="{{asset('/images/insta/5.jpeg')}}" alt="instagram" class="img-fluid">--}}
                        {{--                        </div>--}}
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="contacts">
        <div class="container-xl">
            <div class="row align-items-center">
                <div onclick="window.location.href = '/'"
                     class="col-xxl-2 col-md-2 mb-5 mb-md-0 text-center text-md-left">
                    <img class="cursor-pointer" src="{{asset('img/cezo_logo.svg')}}" alt="Cezomobila">
                </div>
                <div class="col-xxl-3 col-md-4 mb-3 mb-md-0">
                    <i class="fas fa-phone mr-2"></i> <a href="tel:+373 68 39 30 39">+373 68 39 30 39</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a
                        href="tel:+373 68 39 20 39">+373 68 39 20 39</a>
                </div>
                <div class="col-xxl-2 col-md-3 mb-3 mb-md-0 d-block justify-content-md-center secondary_font">
                    <a href="mailto:cezomobila.md@gmail.com"><i class="fas fa-envelope mr-2"></i>
                        cezomobila.md@gmail.com</a>
                </div>
                <div class="col-xxl-3 col-md-3 mb-3 mb-md-0 d-flex justify-content-md-end">
                    <a href="#"><i class="fas fa-map-marker-alt mr-2"></i>str. Calea Orheiului 122, et. 5, of. 5</a>
                </div>
                <div class="col-xxl-2 col-md-3 col-6 text-right d-none d-xxl-block">
                    <div class="social d-flex justify-content-end flex-column flex-md-row">
                        <span class="follow d-block">{{translation('URMĂREȘTE-NE', 'ПОДПИСЫВАЙТЕСЬ НА НАС')}}</span>
                        <ul class="list">
                            <li><a href="https://www.facebook.com/cezomobila.md/"><i class="fab fa-facebook-f"></i></a>
                            </li>
                            <li><a href="https://www.instagram.com/cezomobila.md/"><i class="fab fa-instagram"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-xxl-12 col-md-7 col-6">
                    <nav class="justify-content-center d-flex">
                        <ul>
                            <li class="mb-3 mb-md-0"><a href="/about">{{translation('Despre Noi', 'О нас')}} </a></li>
                            <li class="mb-3 mb-md-0"><a href="/contacts">{{translation('CONTACTE', 'КОНТАКТЫ')}}</a>
                            </li>
                            <li class="mb-3 mb-md-0"><a href="/blog">{{translation('BLOG', 'БЛОГ')}}</a></li>
                            <li class="mb-3 mb-md-0"><a
                                    href="/info/comanda-si-livrare">{{translation('Comanda si livrare', 'Заказ и доставка')}}</a>
                            </li>
                            <li class="mb-3 mb-md-0"><a
                                    href="/info/termeni-si-conditii">{{translation('Termeni si conditii', 'Условия и положения')}}</a>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="col-md-5 col-6 text-right d-block d-xxl-none">
                    <div class="social d-flex justify-content-end flex-column flex-md-row">
                        <span class="follow d-block">{{translation('URMĂREȘTE-NE', 'ПОДПИСЫВАЙТЕСЬ НА НАС')}}</span>
                        <ul class="list">
                            <li><a href="https://www.facebook.com/cezomobila.md/"><i class="fab fa-facebook-f"></i></a>
                            </li>
                            <li><a href="https://www.instagram.com/cezomobila.md/"><i class="fab fa-instagram"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="text-center">
        &copy; {{translation('CEZO Mobila 2021. Toate drepturile rezervate.', 'CEZO Mobila 2021. Все права защищены.')}}</div>
</footer>
