<header class="header d-none d-lg-block d-md-block">
    <div class="container-xl">
        <div class="row align-items-center">
            <div class="col-md-4">
                <a class="navbar-brand" href="/">
                    <img src="{{asset('img/cezo_logo.svg')}}" loading="lazy" class="img-fluid" alt="Cezomobila">
                </a>
                <div class="adress_dropdown">
                    <a id="navbarDropdown" class="dropdown-toggle bold_me" role="button" data-toggle="dropdown" href="#">068 39
                        30 39</a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item bold_me dropdown_item_space" href="tel:+373068393039">068 39 30 39</a>
                        <a class="dropdown-item bold_me dropdown_item_space" href="tel:+373068392039">068 39 20 39</a>
                        <a class="dropdown-item bold_me dropdown_item_space" href="mail:cezomobila.md@gmail.com">cezomobila.md@gmail.com</a>
                        <a class="dropdown-item bold_me dropdown_item_space" href="map:str. Calea Orheiului 122, et. 5, of. 5">str. Calea Orheiului 122, et. 5, of. 5</a>
                    </div>
                </div>
            </div>
{{--            <search-bar-desktop type="desktop"></search-bar-desktop>--}}
            <search-bar-dinamic type="desktop" :lang="{{json_encode(app()->getLocale())}}"></search-bar-dinamic>
            <div class="col-md-2">
                <div class="cart d-flex justify-content-end">
                    <a href="/cart" class="cart_icon d-block">
                        <svg xmlns="http://www.w3.org/2000/svg" width="17.5" height="20" viewBox="0 0 17.5 20">
                            <path id="shopping-bag"
                                  d="M13.75,5a5,5,0,0,0-10,0H0V16.875A3.125,3.125,0,0,0,3.125,20h11.25A3.125,3.125,0,0,0,17.5,16.875V5Zm-5-3.75A3.754,3.754,0,0,1,12.5,5H5A3.754,3.754,0,0,1,8.75,1.25Zm7.5,15.625a1.877,1.877,0,0,1-1.875,1.875H3.125A1.877,1.877,0,0,1,1.25,16.875V6.25h2.5V8.125a.625.625,0,0,0,1.25,0V6.25h7.5V8.125a.625.625,0,0,0,1.25,0V6.25h2.5Z"/>
                        </svg>
                        <cart-count></cart-count>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="top_menu">
        <div class="container-xl">
            <div class="row">
                <div class="col-md-auto m-auto">
                    <header-menu :categories="{{json_encode(categories())}}" :lang="{{json_encode(app()->getLocale())}}"></header-menu>
{{--                    <nav>--}}

{{--                        <ul>--}}
{{--                            <li>--}}
                                {{--                                <a href="#" @click.prevent="menu = !menu">--}}
                                {{--                                    <i id="nav_trigger" class="nav_trigger fas fa-bars"></i>--}}
                                {{--                                    {{translation('Toate Categoriile', 'Все категории')}}</a>--}}

{{--                            </li>--}}
{{--                            <li><a href="/info/comanda-si-livrare">{{translation('Livrare', 'Доставка')}}</a></li>--}}
{{--                            <li><a href="/about">{{translation('Despre noi', 'О нас')}}</a></li>--}}
{{--                            <li><a href="/contacts">{{translation('Contacte', 'Контакты')}}</a></li>--}}
{{--                        </ul>--}}
{{--                    </nav>--}}
                </div>
                <div id="cezo_submenu" class="submenu d-none">
                    <ul>
                        @foreach(categories() as $category)
                            <li class="d-flex align-items-center justify-content-between">
                                <div>
                                    <img src="{{asset($category->icon)}}" alt="">
                                    <a href="/catalog?category_id={{$category->id}}">{{$category->lang->name}}</a>
                                </div>
                                @if(count($category->childs) > 0)
                                    <i class="fa fa-chevron-right"></i>
                                    <ul class="sub_submenu">
                                        @foreach($category->childs as $child)
                                            <li><i class="fa fa-chevron-right"></i>
                                                <a href="/catalog?category_id={{$child->id}}">{{$child->lang->name}}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                @endif
                            </li>
                        @endforeach
                    </ul>
                </div>
                <div class="col">
                    <div class="additional_nav d-flex justify-content-end">
                        <ul>
                            <li><a href="{{url('/catalog?discount=1')}}">{{translation('Reduceri', 'Скидки')}}</a></li>
                            <li>
                                @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                                    @if(app()->getLocale() !== $localeCode)
                                        <a class="text-uppercase"
                                           href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}"
                                           data-language="{{$localeCode}}">
                                            <div class="d-flex align-items-baseline">
                                                @if($localeCode === 'ru')
                                                    <svg xmlns="http://www.w3.org/2000/svg" id="flag-icon-css-ru"
                                                         viewBox="0 0 640 480" width="15.22px">
                                                        <g fill-rule="evenodd" stroke-width="1pt">
                                                            <path fill="#F5F5F5" d="M0 0h640v480H0z"/>
                                                            <path fill="#0039a6" d="M0 160h640v320H0z"/>
                                                            <path fill="#d52b1e" d="M0 320h640v160H0z"/>
                                                        </g>
                                                    </svg>
                                                @else
                                                    <svg xmlns="http://www.w3.org/2000/svg" id="flag-icon-css-ro"
                                                         viewBox="0 0 640 480" width="15.22px">
                                                        <g fill-rule="evenodd" stroke-width="1pt">
                                                            <path fill="#00319c" d="M0 0h213.3v480H0z"/>
                                                            <path fill="#ffde00" d="M213.3 0h213.4v480H213.3z"/>
                                                            <path fill="#de2110" d="M426.7 0H640v480H426.7z"/>
                                                        </g>
                                                    </svg>
                                                @endif
                                                <span class="ml-1"> {{$localeCode}}</span>
                                            </div>
                                        </a>
                                    @endif
                                @endforeach
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<header class="header header_mobile d-lg-none d-md-none">
    <div class="top_bar">
        <div class="container-xl">
            <div class="row d-flex align-items-center justify-content-between">
{{--                <search-bar></search-bar>--}}
                <search-bar-dinamic type="mobile" :lang="{{json_encode(app()->getLocale())}}"></search-bar-dinamic>
                <div class="col-3">
                    <div class="additional_nav d-flex justify-content-end">
                        <ul>
                            <li class="nav-item">
                                @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                                    @if(app()->getLocale() !== $localeCode)
                                        <a class="text-uppercase"
                                           href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}"
                                           data-language="{{$localeCode}}">
                                            <div class="d-flex align-items-baseline">
                                                @if($localeCode === 'ru')
                                                    <svg xmlns="http://www.w3.org/2000/svg" id="flag-icon-css-ru"
                                                         viewBox="0 0 640 480" width="15.22px">
                                                        <g fill-rule="evenodd" stroke-width="1pt">
                                                            <path fill="#F5F5F5" d="M0 0h640v480H0z"/>
                                                            <path fill="#0039a6" d="M0 160h640v320H0z"/>
                                                            <path fill="#d52b1e" d="M0 320h640v160H0z"/>
                                                        </g>
                                                    </svg>
                                                @else
                                                    <svg xmlns="http://www.w3.org/2000/svg" id="flag-icon-css-ro"
                                                         viewBox="0 0 640 480" width="15.22px">
                                                        <g fill-rule="evenodd" stroke-width="1pt">
                                                            <path fill="#00319c" d="M0 0h213.3v480H0z"/>
                                                            <path fill="#ffde00" d="M213.3 0h213.4v480H213.3z"/>
                                                            <path fill="#de2110" d="M426.7 0H640v480H426.7z"/>
                                                        </g>
                                                    </svg>
                                                @endif
                                                <span class="ml-1"> {{$localeCode}}</span>
                                            </div>
                                        </a>
                                    @endif
                                @endforeach
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="top_mob_menu">
        <div class="container-xl">
            <div class="row justify-content-between align-items-center">
                <div class="col-3">
                    <div class="top_menu" @click="mob_nav = !mob_nav">
                        <a href="#"><i id="mob_nav_trigger" class="nav_trigger fas fa-bars"></i></a>
                    </div>
                </div>
                <div class="col-6 d-flex justify-content-center">
                    <a class="navbar-brand" href="/">
                        <img src="{{asset('img/cezo_logo.svg')}}" loading="lazy" class="img-fluid" alt="Cezomobila">
                    </a>
                </div>
                <div class="col-3">
                    <div class="cart d-flex justify-content-end">
                        <a href="/cart" class="cart_icon d-block">
                            <svg xmlns="http://www.w3.org/2000/svg" width="17.5" height="20" viewBox="0 0 17.5 20">
                                <path id="shopping-bag"
                                      d="M13.75,5a5,5,0,0,0-10,0H0V16.875A3.125,3.125,0,0,0,3.125,20h11.25A3.125,3.125,0,0,0,17.5,16.875V5Zm-5-3.75A3.754,3.754,0,0,1,12.5,5H5A3.754,3.754,0,0,1,8.75,1.25Zm7.5,15.625a1.877,1.877,0,0,1-1.875,1.875H3.125A1.877,1.877,0,0,1,1.25,16.875V6.25h2.5V8.125a.625.625,0,0,0,1.25,0V6.25h7.5V8.125a.625.625,0,0,0,1.25,0V6.25h2.5Z"/>
                            </svg>
                            <cart-count></cart-count>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="mob_sub_menu" :class="{'active' : mob_nav}">
        <div class="container-xl">
            <div class="row">
                <div @click="mob_nav = !mob_nav" class="col-12 d-flex align-content-center justify-content-end">
                    <a href="#" id="mob_nav_close" class="btn_main btn__circle"><i class="fa fa-close"></i></a>
                </div>
            </div>
            <div class="row align-items-center">
                <div class="col">
                    <a class="navbar-brand" href="/">
                        <img src="{{asset('img/cezo_logo.svg')}}" loading="lazy" class="img-fluid" alt="Cezomobila">
                    </a>
                </div>
                <div class="col d-flex justify-content-end">
                    <ul>
                        <li class=" nav-item">
                            @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                                @if(app()->getLocale() !== $localeCode)
                                    <a class="text-uppercase"
                                       href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}"
                                       data-language="{{$localeCode}}">
                                        <div class="d-flex align-items-baseline">
                                            @if($localeCode === 'ru')
                                                <svg xmlns="http://www.w3.org/2000/svg" id="flag-icon-css-ru"
                                                     viewBox="0 0 640 480" width="15.22px">
                                                    <g fill-rule="evenodd" stroke-width="1pt">
                                                        <path fill="#F5F5F5" d="M0 0h640v480H0z"/>
                                                        <path fill="#0039a6" d="M0 160h640v320H0z"/>
                                                        <path fill="#d52b1e" d="M0 320h640v160H0z"/>
                                                    </g>
                                                </svg>
                                            @else
                                                <svg xmlns="http://www.w3.org/2000/svg" id="flag-icon-css-ro"
                                                     viewBox="0 0 640 480" width="15.22px">
                                                    <g fill-rule="evenodd" stroke-width="1pt">
                                                        <path fill="#00319c" d="M0 0h213.3v480H0z"/>
                                                        <path fill="#ffde00" d="M213.3 0h213.4v480H213.3z"/>
                                                        <path fill="#de2110" d="M426.7 0H640v480H426.7z"/>
                                                    </g>
                                                </svg>
                                            @endif
                                            <span class="ml-1"> {{$localeCode}}</span>
                                        </div>
                                    </a>
                                @endif
                            @endforeach
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <nav>
                    <mobile-categories :categories="{{json_encode(categories())}}"
                                       :lang="{{json_encode(app()->getLocale())}}"></mobile-categories>
                </nav>
            </div>
        </div>
    </div>
</header>
