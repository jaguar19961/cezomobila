@extends('front.layouts.app')
@section('content')
<section>
	<div class="magazin_header">
		<div class="container-xl">
			<div class="row">
				<div class="col-12"><h1>{{$page->lang->name}}</h1></div>
			</div>
		</div>
	</div>
</section>
<section>
	<div class="container-xl">
		<div class="row">
			<div class="col-md-12">
				{!! $page->lang->description !!}
			</div>
		</div>
	</div>
</section>
@endsection
