@extends('front.layouts.app')
@section('content')
    <section>
        <div class="magazin_header">
            <div class="container-xl">
                <div class="row">
                    <div class="col-12"><h1>{!! $model->lang->name !!}</h1></div>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="container-xl">
            <div class="row">
                <img src="{{asset('img/CEZO.svg')}}" alt="Cezomobila" class="deco_logo">
                <div class="col-md-6">
                    <h4 class="mb-3">{!! $model->lang->meta_name !!}</h4>
	                {!! $model->lang->meta_description !!}
                    <img src="{{asset($model->image_1)}}" alt="about" class="img-fluid">
                </div>
                <div class="col-md-6 pl-md-5 pt-md-0 pt-4">
                    <div class="row">
                        <div class="col-6">
                            <img src="{{asset($model->image_2)}}" alt="about" class="img-fluid">
                        </div>
                        <div class="col-6">
                            <img src="{{asset($model->image_3)}}" alt="about" class="img-fluid">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-md-5">
                <div class="col-md-6">
                    <img src="{{asset($model->image_4)}}" alt="about" class="img-fluid">
                </div>
                <div class="col-md-6 mt-5">
                    {!! $model->lang->delivery !!}
                </div>
            </div>
        </div>
    </section>
@endsection
