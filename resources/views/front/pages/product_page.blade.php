@extends('front.layouts.app')
@section('content')
    <section class="breadcrumbs product border-top-0 p-0">
        <div class="container-xl">
            <div class="row">
                <div class="col-12">
                    <div>
                        <ul>
                            <li>
                                <a href="/">{{app()->getLocale() === 'ro' ? 'Principala' : 'Главная'}}</a>
                            </li>
                            {{--                            <li>--}}
                            {{--                                <a href="/catalog">{{app()->getLocale() === 'ro' ? 'Catalog' : 'Каталог' }}</a>--}}
                            {{--                            </li>--}}
                            @if($product->category->parent !== null)
                                <li>
                                    <a href="#">{{$product->category->parent->lang->name}}</a>
                                </li>
                            @endif
                            <li>
                                <a href="/catalog?category_id={{$product->category->id}}">{{ $product->category->lang->name }}</a>
                            </li>
                            <li class="active">
                                <a href="#">{{ $product->name}}</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <product :product="{{json_encode($product)}}"
             :credits="{{json_encode($credits)}}"
             :specifications="{{json_encode($specifications)}}"
             :lang="{{json_encode(app()->getLocale())}}"
             :filters="{{json_encode($filterData)}}"></product>

    {{--    test--}}
    @if($products_upsells->count() > 0)
        <upsells :upsells_prices="{{json_encode($upsells_prices)}}"
                 :products_upsells="{{json_encode($products_upsells)}}"
                 :lang="{{json_encode(app()->getLocale())}}"/>
    @endif
    <section class="recenzii">
        <div class="container-xl">
            @if(count($reviews) > 0)
                <div class="row mt-md-5">
                    <div class="col-md-12">
                        <div class="more_button text-center">
                            <a href="#"
                               class="btn_main btn__transparent btn__square">{{translation('Recenzii', 'Отзывы')}}
                                ( <span class="secondary_font">{{count($product->rates)}}</span> )</a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    @foreach($reviews as $review)
                        <div class="col-12 col-md-4 d-flex">
                            <review-item :item="{{json_encode($review)}}"></review-item>
                        </div>
                    @endforeach
                </div>
            @endif
            <div class="row mt-5">
                <div class="col-md-12 text-center">
                    <a href="#" @click.prevent="showMore = !showMore"
                       class="btn_main">{{translation('SCRIE ȘI TU OPINIA TA ...', 'НАПИШИТЕ СВОЕ МНЕНИЕ ...')}}</a>
                </div>
            </div>
        </div>
        <div v-if="showMore" class="container-xl">
            <div class="row">
                <div class="col-md-6 offset-md-3">
                    <h5 class="text-center mb-3 mt-5">{{translation('Scrie o recenzie', 'Напишите отзыв')}}</h5>
                    <review-form :product_id="{{json_encode($product->id)}}"></review-form>
                </div>
            </div>
        </div>
    </section>
    <section class="mt-5">
        <div class="container-xl">
            <h2 class="text-center mb-5">{{translation('Produse Similare', 'Похожие продукты')}}</h2>
            <div class="row no-gutters">
                @foreach($similars as $item)
                    <div class="col-6 col-sm-6 col-md-3 col-lg-3 d-flex">
                        <product-item :product="{{json_encode($item)}}"
                                      :lang="{{json_encode(app()->getLocale())}}"></product-item>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@endsection
