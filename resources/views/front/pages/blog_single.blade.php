@extends('front.layouts.app')
@section('content')
<section class="blog-single_header__wrapper pt-0 d-none d-md-block">
	<div class="blog-single_header">
		<div class="container-fluid text-center">
			<div class="row">
				<div class="col-12"><h2>{{translation('DECORAȚIE', 'УКРАШЕНИЕ')}}</h2></div>
			</div>
		</div>
	</div>
</section>
<section class="blog_single">
	<div class="container-xl">
		<div class="row">
			<div class="col-md-12">
				<div class="badge mb-3">{{translation('DECORAȚIE', 'УКРАШЕНИЕ')}}</div>
				<h1>{{$blog->lang->name}}</h1>
				<div class="meta">
					<ul>
						<li><small>{{$blog->created_at->format('d M Y')}}</small></li>
{{--						<li>--}}
{{--							<svg xmlns="http://www.w3.org/2000/svg" width="12.724" height="11.251" viewBox="0 0 12.724 11.251">--}}
{{--								<path id="Icon_awesome-comment" data-name="Icon awesome-comment" d="M5.858,2.25C2.622,2.25,0,4.38,0,7.009A4.224,4.224,0,0,0,1.3,10,5.821,5.821,0,0,1,.05,12.192a.182.182,0,0,0-.034.2.179.179,0,0,0,.167.11A5.233,5.233,0,0,0,3.4,11.325a6.978,6.978,0,0,0,2.457.444c3.235,0,5.858-2.13,5.858-4.759S9.093,2.25,5.858,2.25Z" transform="translate(0.509 -1.75)" fill="none" stroke="#22262a" stroke-width="1"/>--}}
{{--						  </svg>--}}
{{--						  <small>5 commentarii</small>--}}
{{--						  </li>--}}
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="container mt-3">
		<div class="row">
			<div class="col-md-12">
                {!! $blog->lang->description !!}
            </div>
		</div>
	</div>
</section>
<section>
	<div class="container-xl">
		<div class="row">
			<div class="col-md-12 text-center">
				<div class="social d-inline-flex flex-row align-items-center">
					<p class="mb-0">{{translation('Distribuie:', 'Поделиться:')}}</p>
					<ul class="list">
						<li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
						<li><a href="#"><i class="fab fa-instagram"></i></a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection
