@extends('front.layouts.app')
@section('content')
    <catalog :brands="{{json_encode($brands)}}"
             :attributes="{{json_encode($attributes)}}"
             :categories="{{json_encode($categories)}}"
             :props="{{json_encode($props)}}"
             :current_category="{{json_encode($current_category)}}"
             :lang="{{json_encode(app()->getLocale())}}"
             :upsells_prices="{{json_encode($upsells_prices)}}"
             :products_upsells="{{json_encode($products_upsells)}}"></catalog>

@endsection
