@extends('front.layouts.app')
@section('content')
<section>
	<div class="magazin_header">
		<div class="container-xl">
			<div class="row">
				<div class="col-12"><h1>{{translation('Livrare si verificare', 'Доставка и проверка')}}</h1></div>
			</div>
		</div>
	</div>
</section>
<section class="steps_progress">
	<div class="container-xl">
		<div class="row no-gutters">
			<div class="col-12 col-md-4 pb-3 pb-md-0">
				<div class="step d-flex pb-3">
					<div class="pr-2 check_mark">
						<svg xmlns="http://www.w3.org/2000/svg" width="14.883" height="14.732" viewBox="0 0 14.883 14.732">
							<g id="Icon_feather-check-circle" data-name="Icon feather-check-circle" transform="translate(0.675 0.64)">
							 	<path id="Контур_53" data-name="Контур 53" d="M16.5,9.124v.621a6.751,6.751,0,1,1-4-6.17" transform="translate(-3 -2.99)" fill="none" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-width="1"/>
								<path id="Контур_54" data-name="Контур 54" d="M22.276,6l-6.751,6.757L13.5,10.732" transform="translate(-8.775 -4.645)" fill="none" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-width="1"/>
							</g>
						</svg>
					</div>
					<div>
                        <h4>{{translation('Cosul de cumparaturi', 'Корзина покупок')}}</h4>
                        <span>{{translation('Poduse selectate', 'Выбранные продукты')}}</span>
					</div>
				</div>
			</div>
			<div class="col-12 col-md-4 pb-3 pb-md-0">
				<div class="step d-flex pb-3 active">
					<div class="pr-2 check_mark">
						<svg xmlns="http://www.w3.org/2000/svg" width="14.883" height="14.732" viewBox="0 0 14.883 14.732">
							<g id="Icon_feather-check-circle" data-name="Icon feather-check-circle" transform="translate(0.675 0.64)">
							 	<path id="Контур_53" data-name="Контур 53" d="M16.5,9.124v.621a6.751,6.751,0,1,1-4-6.17" transform="translate(-3 -2.99)" fill="none" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-width="1"/>
								<path id="Контур_54" data-name="Контур 54" d="M22.276,6l-6.751,6.757L13.5,10.732" transform="translate(-8.775 -4.645)" fill="none" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-width="1"/>
							</g>
						</svg>
					</div>
					<div>
                        <h4>{{translation('Livrare și verificare', 'Доставка и проверка')}}</h4>
                        <span>{{translation('Introduceti datele', 'Введите данные')}}</span>
					</div>
				</div>
			</div>
			<div class="col-12 col-md-4">
				<div class="step d-flex pb-3">
					<div class="pr-2 check_mark">
						<svg xmlns="http://www.w3.org/2000/svg" width="14.883" height="14.732" viewBox="0 0 14.883 14.732">
							<g id="Icon_feather-check-circle" data-name="Icon feather-check-circle" transform="translate(0.675 0.64)">
							 	<path id="Контур_53" data-name="Контур 53" d="M16.5,9.124v.621a6.751,6.751,0,1,1-4-6.17" transform="translate(-3 -2.99)" fill="none" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-width="1"/>
								<path id="Контур_54" data-name="Контур 54" d="M22.276,6l-6.751,6.757L13.5,10.732" transform="translate(-8.775 -4.645)" fill="none" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-width="1"/>
							</g>
						</svg>
					</div>
					<div>
                        <h4>{{translation('Confirmare', 'Подтверждение')}}</h4>
                        <span>{{translation('Previzualizaţi comanda', 'Предварительный просмотр заказа')}}</span>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<cart-delivery :locals="{{json_encode($locals)}}" :lang="{{json_encode(app()->getLocale())}}"></cart-delivery>
@endsection
