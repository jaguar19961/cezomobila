@extends('front.layouts.app')
@section('content')
    <section>
        <div class="magazin_header">
            <div class="container-xl">
                <div class="row">
                    <div class="col-12"><h1>{{translation('Confirmare', 'Подтверждение')}}</h1></div>
                </div>
            </div>
        </div>
    </section>
    <section class="steps_progress">
        <div class="container-xl">
            <div class="row no-gutters">
                <div class="col-12 col-md-4 pb-3 pb-md-0">
                    <div class="step d-flex pb-3">
                        <div class="pr-2 check_mark">
                            <svg xmlns="http://www.w3.org/2000/svg" width="14.883" height="14.732"
                                 viewBox="0 0 14.883 14.732">
                                <g id="Icon_feather-check-circle" data-name="Icon feather-check-circle"
                                   transform="translate(0.675 0.64)">
                                    <path id="Контур_53" data-name="Контур 53"
                                          d="M16.5,9.124v.621a6.751,6.751,0,1,1-4-6.17" transform="translate(-3 -2.99)"
                                          fill="none" stroke="#000" stroke-linecap="round" stroke-linejoin="round"
                                          stroke-width="1"/>
                                    <path id="Контур_54" data-name="Контур 54" d="M22.276,6l-6.751,6.757L13.5,10.732"
                                          transform="translate(-8.775 -4.645)" fill="none" stroke="#000"
                                          stroke-linecap="round" stroke-linejoin="round" stroke-width="1"/>
                                </g>
                            </svg>
                        </div>
                        <div>
                            <h4>{{translation('Cos de cumparaturi', 'Корзина покупок')}}</h4>
                            <span>{{translation('Poduse selectate', 'Выбранные продукты')}}</span>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-4 pb-3 pb-md-0">
                    <div class="step d-flex pb-3">
                        <div class="pr-2 check_mark">
                            <svg xmlns="http://www.w3.org/2000/svg" width="14.883" height="14.732"
                                 viewBox="0 0 14.883 14.732">
                                <g id="Icon_feather-check-circle" data-name="Icon feather-check-circle"
                                   transform="translate(0.675 0.64)">
                                    <path id="Контур_53" data-name="Контур 53"
                                          d="M16.5,9.124v.621a6.751,6.751,0,1,1-4-6.17" transform="translate(-3 -2.99)"
                                          fill="none" stroke="#000" stroke-linecap="round" stroke-linejoin="round"
                                          stroke-width="1"/>
                                    <path id="Контур_54" data-name="Контур 54" d="M22.276,6l-6.751,6.757L13.5,10.732"
                                          transform="translate(-8.775 -4.645)" fill="none" stroke="#000"
                                          stroke-linecap="round" stroke-linejoin="round" stroke-width="1"/>
                                </g>
                            </svg>
                        </div>
                        <div>
                            <h4>{{translation('Livrare și verificare', 'Доставка и проверка')}}</h4>
                            <span>{{translation('Introduceti datele', 'Введите данные')}}</span>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="step d-flex pb-3 active">
                        <div class="pr-2 check_mark">
                            <svg xmlns="http://www.w3.org/2000/svg" width="14.883" height="14.732"
                                 viewBox="0 0 14.883 14.732">
                                <g id="Icon_feather-check-circle" data-name="Icon feather-check-circle"
                                   transform="translate(0.675 0.64)">
                                    <path id="Контур_53" data-name="Контур 53"
                                          d="M16.5,9.124v.621a6.751,6.751,0,1,1-4-6.17" transform="translate(-3 -2.99)"
                                          fill="none" stroke="#000" stroke-linecap="round" stroke-linejoin="round"
                                          stroke-width="1"/>
                                    <path id="Контур_54" data-name="Контур 54" d="M22.276,6l-6.751,6.757L13.5,10.732"
                                          transform="translate(-8.775 -4.645)" fill="none" stroke="#000"
                                          stroke-linecap="round" stroke-linejoin="round" stroke-width="1"/>
                                </g>
                            </svg>
                        </div>
                        <div>
                            <h4>{{translation('Confirmare', 'Подтверждение')}}</h4>
                            <span>{{translation('Previzualizaţi comanda', 'Предварительный просмотр заказа')}}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container-xl">
            <div class="row">
                <div class="col-md-6 offset-md-3 text-center">
                    <h2 class="mb-3"
                        style="font-weight: 600;">{{translation('VĂ MULȚUMIM PENTRU CUMPĂRĂTURI!', 'СПАСИБО ЗА ПОКУПКИ!')}}</h2>
                    <p>{{translation('Comanda dvs. a fost procesată cu succes!', 'Ваш заказ успешно обработан!')}}</p>
                    <div class="grey_block bordered mb-3">
                        <span style="font-weight: 600;">{{translation('Numărul comenzii:', 'Номер заказа:')}} <span
                                class="secondary_font" style="color:red;">#{{$invoice_id}}</span></span>
                    </div>
                    <p style="line-height: 2;">{{translation('În curând veți fi contactat de operatorul nostru pentru a vă confirma comanda.
					Un e-mail de confirmare a fost trimis la adresa de e-mail furnizată.
					Dacă nu ați primit confirmarea în decurs de o oră, vă rugăm să ne contactați.', 'Вскоре с вами свяжется наш оператор для подтверждения вашего заказа.
На указанный адрес электронной почты было отправлено письмо с подтверждением.
Если вы не получили подтверждения в течение часа, свяжитесь с нами.')}}</p>
                </div>
            </div>
        </div>
    </section>
@endsection
