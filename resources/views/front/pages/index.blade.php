@extends('front.layouts.app')
@section('content')
    <section class="banner">
        <div class="top_banner">
            <div class="container">
                <el-carousel indicator-position="outside" arrow="always">
                    @foreach($galleries as $key => $gallery)
                        <el-carousel-item>
                            <app-image :lazy-src="{{json_encode($gallery->image)}}"
                                       :lazy-srcset="{{json_encode($gallery->image)}}"
                                       alt="banner">
                            </app-image>
                            <div class="banner_cta d-flex flex-column
                                @if($loop->iteration % 2 == 0)
                                    aligned_right align-items-end
                                @else
                                    aligned_left align-items-start
                                @endif">
                                <div>{{$gallery->lang->name}}</div>
                                @if(!empty($gallery->link))
                                    <a href="{{url($gallery->link)}}"
                                       class="btn_main btn__square btn__large header_btn">{{translation('Vezi mai multe', 'Узнать больше')}}
                                        <i class="fa fa-chevron-right pl-2"></i>
                                    </a>
                                @endif
                            </div>
                        </el-carousel-item>
                    @endforeach
                </el-carousel>
            </div>
        </div>
        <div class="top_contacte">
            <div class="container-xl">
                <div class="row d-flex align-items-center">
                    <div class="col">
                        <a href="mailto:cezomobila.md@gmail.com" class="mail">cezomobila.md@gmail.com</a>
                    </div>
                    <div class="col">
                        <div class="social d-flex justify-content-end">
                            <ul class="list">
                                <li><span
                                        class="follow d-none d-md-inline d-lg-inline">{{translation('URMĂREȘTE-NE', 'ПОДПИСЫВАЙТЕСЬ НА НАС')}}</span>
                                </li>
                                <li><a href="https://www.facebook.com/cezomobila.md/"><i class="fab fa-facebook-f"></i></a>
                                </li>
                                <li><a href="https://www.instagram.com/cezomobila.md/"><i class="fab fa-instagram"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {{--    <section class="navigation d-none d-md-block d-lg-block">--}}
    {{--        <div class="container-xl">--}}
    {{--            <categories :categories="{{json_encode(categories())}}"></categories>--}}
    {{--        </div>--}}
    {{--    </section>--}}
    @if($promotions->count() > 0)
    <section class="promotie">
        <h2 class="text-center mb-5">{{translation('Produse la promotie', 'Товары со скидкой')}}</h2>
        <div class="container-xl">
            <div class="row no-gutters">
                @foreach($promotions as $item)
                    <div class="col-6 col-sm-6 col-md-3 col-lg-3 d-flex">
                        <product-item :product="{{json_encode($item)}}"
                                      :lang="{{json_encode(app()->getLocale())}}"></product-item>
                    </div>
                @endforeach
            </div>
            {{--            <div class="row mt-5">--}}
            {{--                <div class="col-md-12">--}}
            {{--                    <div class="more_button text-center">--}}
            {{--                        <a href="#" class="btn_main btn__transparent">--}}
            {{--								<span class="icon">--}}
            {{--									<svg xmlns="http://www.w3.org/2000/svg" width="10.95" height="10.964"--}}
            {{--                                         viewBox="0 0 10.95 10.964">--}}
            {{--										<path id="Icon_open-reload" data-name="Icon open-reload"--}}
            {{--                                              d="M5.482,0A5.482,5.482,0,1,0,9.374,9.374l-.987-.987A4.114,4.114,0,1,1,5.468,1.37,3.987,3.987,0,0,1,8.332,2.618L6.839,4.111H10.95V0L9.319,1.631A5.451,5.451,0,0,0,5.468,0Z"/>--}}
            {{--									</svg>--}}
            {{--								</span>--}}
            {{--                            AFIȘEAZĂ MAI MULT</a>--}}
            {{--                    </div>--}}
            {{--                </div>--}}
            {{--            </div>--}}
        </div>
    </section>
    @endif
    @if($populars->count() > 0)
    <section class="promotie">
        <h2 class="text-center mb-5">{{translation('Produse populare', 'Популярные товары')}}</h2>
        <div class="container-xl">
            <div class="row no-gutters">
                @foreach($populars as $item)
                    <div class="col-6 col-sm-6 col-md-3 col-lg-3 d-flex">
                        <product-item :product="{{json_encode($item)}}"
                                      :lang="{{json_encode(app()->getLocale())}}"></product-item>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
    @endif
    @if($new->count() > 0)
    <section class="promotie">
        <h2 class="text-center mb-5">{{translation('Produse noi', 'Новые продукты')}}</h2>
        <div class="container-xl">
            <div class="row no-gutters">
                @foreach($new as $item)
                    <div class="col-6 col-sm-6 col-md-3 col-lg-3 d-flex">
                        <product-item :product="{{json_encode($item)}}"
                                      :lang="{{json_encode(app()->getLocale())}}"></product-item>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
    @endif
    {{--    <section class="mid_banner">--}}
    {{--        <div class="container-xl">--}}
    {{--            <div class="row">--}}
    {{--                <div class="col">--}}
    {{--                    <img src="img/mid_banner.png" alt="Wow" class="img-fluid">--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--    </section>--}}
    @if($reviews->count() > 0)
        <section class="recenzii">
            <h2 class="text-center mb-5">{{translation('Recenzii', 'Отзывы')}}</h2>
            <div class="container-xl">
                <div class="row">
                    @foreach($reviews as $review)
                        <div class="col-12 col-md-4 d-flex">
                            <review-item :item="{{json_encode($review)}}"></review-item>
                        </div>
                    @endforeach
                </div>
                {{--            <div class="row mt-5">--}}
                {{--                <div class="col-md-12">--}}
                {{--                    <div class="more_button text-center">--}}
                {{--                        <a href="#" class="btn_main btn__transparent">--}}
                {{--								<span class="icon">--}}
                {{--									<svg xmlns="http://www.w3.org/2000/svg" width="10.95" height="10.964"--}}
                {{--                                         viewBox="0 0 10.95 10.964">--}}
                {{--										<path id="Icon_open-reload" data-name="Icon open-reload"--}}
                {{--                                              d="M5.482,0A5.482,5.482,0,1,0,9.374,9.374l-.987-.987A4.114,4.114,0,1,1,5.468,1.37,3.987,3.987,0,0,1,8.332,2.618L6.839,4.111H10.95V0L9.319,1.631A5.451,5.451,0,0,0,5.468,0Z"/>--}}
                {{--									</svg>--}}
                {{--								</span>--}}
                {{--                            AFIȘEAZĂ MAI MULT</a>--}}
                {{--                    </div>--}}
                {{--                </div>--}}
                {{--            </div>--}}
            </div>
        </section>
    @endif
@endsection
