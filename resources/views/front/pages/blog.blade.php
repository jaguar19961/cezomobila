@extends('front.layouts.app')
@section('content')
    <section>
        <div class="magazin_header">
            <div class="container-xl">
                <div class="row">
                    <div class="col-12"><h1>{{translation('Blog', 'Блог')}}</h1></div>
                </div>
            </div>
        </div>
    </section>
    <section class="blog">
        <div class="container-xl">
            <div class="row">
                @foreach($blogs as $blog)
                    <div class="col-md-4 mb-5 mb-lg-0 d-flex">
                        <div class="card flex-fill">
                            <img src="{{$blog->image}}" alt="blog1" class="img-fluid">
                            <div class="d-flex justify-content-between align-items-center mb-2 mt-2">
                                <div class="meta">
                                    <ul>
                                        <li><small>{{$blog->created_at->format('d M Y')}}</small></li>
{{--                                        <li>--}}
{{--                                            <svg xmlns="http://www.w3.org/2000/svg" width="12.724" height="11.251"--}}
{{--                                                 viewBox="0 0 12.724 11.251">--}}
{{--                                                <path id="Icon_awesome-comment" data-name="Icon awesome-comment"--}}
{{--                                                      d="M5.858,2.25C2.622,2.25,0,4.38,0,7.009A4.224,4.224,0,0,0,1.3,10,5.821,5.821,0,0,1,.05,12.192a.182.182,0,0,0-.034.2.179.179,0,0,0,.167.11A5.233,5.233,0,0,0,3.4,11.325a6.978,6.978,0,0,0,2.457.444c3.235,0,5.858-2.13,5.858-4.759S9.093,2.25,5.858,2.25Z"--}}
{{--                                                      transform="translate(0.509 -1.75)" fill="none" stroke="#22262a"--}}
{{--                                                      stroke-width="1"/>--}}
{{--                                            </svg>--}}
{{--                                            <small>5 commentarii</small>--}}
{{--                                        </li>--}}
                                    </ul>
                                </div>
                                <div class="meta">
                                    <small>{{translation('Decorație', 'Украшение')}}</small>
                                </div>
                            </div>
                            <h5 class="mb-3 mt-3">{{$blog->lang->name}}</h5>
                            <p>{!! $blog->lang->short_description !!}</p>
                            <a href="{{url('/article/'.$blog->slug)}}"
                               class="btn__light btn_main btn__square btn__lighten">{{translation('Mai detaliat', 'Более подробный')}}</a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@endsection
