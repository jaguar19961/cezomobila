@extends('front.layouts.app')
@section('content')
<section>
	<div class="magazin_header">
		<div class="container-xl">
			<div class="row">
				<div class="col-12"><h1>{{translation('Contacte', 'Контакты')}}</h1></div>
			</div>
		</div>
	</div>
</section>
<section>
	<div class="container-xl">
		<div class="row">
			<div class="col-md-6 order-2 order-md-1 mt-5 mt-md-0">
				<h4 class="mb-2">{!! $model->lang->name !!}</h4>
				<p>{!! $model->lang->meta_name !!}</p>

				<p>{!! $model->lang->meta_description !!}</p>
				<div class="row">
					<div class="col-md-6 mt-5 mt-md-0">
						<h4 class="mb-3">{{translation('Contacte', 'Контакты')}}</h4>
						<ul class="contacte">
							<li>+373 68 39 30 39</li>
							<li>+373 68 39 20 39</li>
							<li>cezomobila.md@gmail.com</li>
						</ul>
					</div>
					<div class="col-md-6 mt-5 mt-md-0">
						<h4 class="mb-3">{{translation('ADRESA NOASTRĂ', 'НАШ АДРЕС')}}</h4>
						<ul class="contacte">
							<li>mun.Chișinău str.Calea Orheiului 122, et.5, of.5</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-md-6 order-1 order-md-2">
				<h4 class="mb-2">{{translation('CONTACTEAZĂ-NE', 'СВЯЖИТЕСЬ С НАМИ')}}</h4>
				<p>{!! $model->lang->delivery !!}</p>
				<form action="POST" class="bordered">
					<div class="form-group row">
						<div class="col-md-6">
							<input type="text" class="form-control" placeholder="{{translation('Nume Prenume', 'Имя Фамилия')}}">
						</div>
						<div class="col-md-6">
							<input type="email" class="form-control" placeholder="{{translation('Emailu dvs', 'Ваш адрес электронной почты')}}">
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<textarea class="form-control" placeholder="{{translation('Mesaj aditional', 'Дополнительное сообщение')}}"></textarea>
						</div>
					</div>
					<div class="row mt-3">
						<div class="col-md-12 text-md-right text-center">
							<input type="submit" class="btn_main" value="{{translation('Trimite', 'Отправить')}}">
						</div>
					</div>
				</form>
			</div>
		</div>
		<div class="row mt-5">
			<div class="col-md-12">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2717.6378108511462!2d28.855859515516496!3d47.06695477915259!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40c97d7b39ef5e55%3A0xb3b9a60b7c32b0b5!2scezomobila.md!5e0!3m2!1sru!2s!4v1618411210644!5m2!1sru!2s" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
			</div>
		</div>
	</div>
</section>
@endsection
