<!-- Emails use the XHTML Strict doctype -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <!-- The character set should be utf-8 -->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width"/>
    <!-- Link to the email's CSS, which will be inlined into the email -->
    <!-- <link rel="stylesheet" href="assets/css/foundation-emails.css"> -->
    <link rel="stylesheet" href="{{env('APP_URL')}}/css/mine-styles.css">
    <style>
        /* FONTS  */
        @font-face {
            font-family: Brandon Medium;
            src: url('https://www.pdpaola.com/themes/pdpaola/fonts/Brandon_bld.woff');
        }

        @font-face {
            font-family: Brandon Light;
            src: url('https://www.pdpaola.com/themes/pdpaola/fonts/Brandon_reg.woff');
        }

        @font-face {
            font-family: Brandon Regular;
            src: url('https://www.pdpaola.com/themes/pdpaola/fonts/Brandon_reg.woff');
        }

        /* PAGE STYLES */
        *:before *:after {
            box-sizing: border-box;
        }

        html, body {
            padding: 0;
            margin: 0;
            color: #1A1919;
            font-size: 18px;
            min-height: 866px;
            max-height: 866px;
            min-width: 834px;
            max-width: 834px;
        }

        /* WHOLE ORDER CONTAINER */
        .main-container {
            margin: 0;
            text-align: center;
            background-color: #EFEFEE;
            padding: 0.1em 0.1em;
        }

        /* LOGO  */
        .logo {
            font-family: 'Brandon Regular', sans-serif;
            letter-spacing: 0.17em;
            color: #1A1919;
        }

        .light-logo-letter {
            font-family: 'Brandon Light', sans-serif;
            font-weight: 100;
        }

        /* INSIDE CONTAINER */
        .order-container {
            background-color: white;
            padding: 1em 1em 0 1em;
            margin: 1em;
        }

        p {
            font-family: 'Brandon Light', sans-serif;
            font-weight: 100;
        }

        td, th {
            font-size: 1em;
            font-family: 'Brandon Light', sans-serif;
            font-weight: 100;
            letter-spacing: 0.05em;
        }

        .second-heading {
            font-family: 'Brandon Medium', sans-serif;
            text-transform: uppercase;
            letter-spacing: 0.05em;
        }

        .details {
            margin: -1em 3em 0 3em;
            padding-bottom: 1em;
        }

        /* TABLE  */
        /* RECAP ORDER TABLE */
        .table-container {
            margin: 0em 2em 0 2em;
        }

        .order-recap {
            border-top: 0.01em solid #1B1A1A;
            font-weight: 700;
            padding: 1em 1em;
            text-align: left;
            text-transform: uppercase;
            width: 100%;
        }

        .order-recap-row {
            margin: 1em;
        }

        .table-h {
            padding: 0.2em;
            font-family: 'Brandon Medium', sans-serif;
            font-weight: 700;
        }

        .table-data {
            font-family: 'Brandon Light', sans-serif;
            font-weight: 100;
            text-align: left;
            width: 32%;
        }

        /* ORDER DETAILS TABLE */
        table.order-details {
            border-collapse: collapse;
            padding: 0em 1em;
            text-align: left;
            text-transform: initial;
            height: 240px;
            width: 100%;
        }

        .details-h {
            border-top: 0.01em solid #1B1A1A;
            border-bottom: 0.01em solid #1B1A1A;
            font-family: 'Brandon Light', sans-serif;
            font-weight: 100;
            padding-left: 0.8em;
        }

        .details-h-one {
            padding-left: 1.5em;
        }

        th:nth-child(2) {
            padding-left: 1.9em;
        }

        .model {
            padding-left: 1.5em;
        }

        .quantities {
            padding-right: 1em;
        }

        tr.first-item-row {
            border-bottom: 0.01em solid #C3C4C3;
            height: 90px;
        }

        td.sold-item-values {
            padding-left: 1.3em;
        }

        td.sold-item-quantity {
            padding-left: 2.5em;
        }

        td.sold-item-price {
            padding-left: 2em;
        }

        .xs-img {
            height: 4em;
        }

        .first-img {
            margin-right: 1em;
        }

        .second-img {
            margin-top: 1em;
        }

        .product-number {
            color: #8A8B8A;
            font-weight: 100;
        }

        tr.second-item-row {
            border-bottom: 0.01em solid #C3C4C3;
            height: 90px;
        }

        /* PRODUCT RECAP  */
        .payment-container {
            margin: 0em 2em 0 2em;
            height: 10em;
        }

        .payment-recap {
            float: left;
            margin-top: 0.8em;
        }

        .col-1 {
            text-align: left;
            margin-left: 62%;
            font-size: 1em;
        }

        .col-2 {
            text-align: right;
            margin-left: 10%;
        }

        .payment-p {
            margin: 0.5em 0;
            letter-spacing: 0.08em;
        }

        .total {
            font-family: 'Brandon Medium', sans-serif;
        }

        /* DELIVERY ADDRESS + INVOICE DETAILS */
        .delivery-container {
            margin: 0em 2em 0 2em;
            border-top: 0.01em solid #1B1A1A;
            border-bottom: 0.01em solid #1B1A1A;
            height: 13em;
        }

        .delivery-col-left {
            float: left;
            text-align: left;
            width: 43%;
            margin-left: 1em;
        }

        .delivery-heading {
            font-family: 'Brandon Medium', sans-serif;
            font-weight: 500;
        }

        .invoice-col-right {
            float: left;
            text-align: left;
            width: 43%;
            margin-left: 10%;
        }

        /* EXTRA ACTIONS */
        .extra-actions-container {
            margin: 0em 2em 0em 2em;
            padding-bottom: 0.5em;
        }

        .order-state {
            font-family: 'Brandon Light', sans-serif;
            font-size: 14px;
            background-color: black;
            color: white;
            text-decoration: none;
            padding: 0.4em 11em;
        }

        .download-order {
            font-family: 'Brandon Light', sans-serif;
            font-weight: 400;
            font-size: 14px;
            background-color: black;
            color: white;
            text-decoration: none;
            padding: 0.4em 13.45em;
        }

        /* RECOMMENDATIONS */
        .recommendations-container {
            margin: -1em 1em -1em 1em;
            padding: 0 1em;
            background-color: #FBF9F9;
            height: 17em;
        }

        .recommendations-col {
            float: left;
            width: 33%;
            padding: 2em 0;
        }

        .recommendations-img {
            height: 8em;
        }

        .recommendations-h {
            font-family: 'Brandon Medium', sans-serif;
            font-weight: 500;
            margin-bottom: -1em;
        }

        /* GOODBYE IMAGE */
        .goodbye-img-container {
            text-align: center;
        }

        .goodbye-img {
            margin: -0.1em 0;
            height: 607px;
        }

        .goodbye-p {
            font-family: 'Brandon Light', sans-serif;
            font-weight: 600;
            font-size: 1em;
            letter-spacing: 0.1em;
            color: white;
            position: relative;
            bottom: 18em;
            right: 2%;
            margin: auto;
        }

        /* SOCIAL LINKS */
        .social-links-container {
            padding: 1em;
            margin-top: -2em;
        }

        .social-logo {
            height: 2em;
            padding: 1em 0.3em;
        }

        a {
            text-decoration: none;
        }
    </style>
</head>

<body>

<!-- WHOLE ORDER CONTAINER -->
<div class="main-container">
    <a href="{{env('APP_URL')}}">
        <h2 class="logo">Cezomobila.md Magazin online de mobila</h2>
    </a>
    <!-- INSIDE CONTAINER -->
    <div class="order-container">
        <h2 class="second-heading"> Confirmare comanda nr. #{{$user_info['invoice_id']}}</h2>
        <!-- ORDER RECAP TABLE -->
        <div class="table-container">
            <table class="order-recap">
                <tr class="order-recap-row">
                    <td class="table-h"> Data generare factura</td>
                    <td class="table-data"> {{$user_info['created_at']->format('d-m-yy H:i')}} </td>
                </tr>
                <!-- second row -->
                <tr class="order-recap-row">
                    <td class="table-h"> Metoda de plata</td>
                    <td class="table-data">{{ $user_info['payment_method']}}</td>
                </tr>
                <tr class="order-recap-row">
                    <td class="table-h"> Metoda de livare</td>
                    <td class="table-data">{{ $user_info['delivery_method']}}</td>
                </tr>
            </table>
        </div>

        <!-- ORDER DETAILS TABLE -->
        <div class="table-container">
            <table class="order-details">
                <tr class="item">
                    <th class="details-h"> Sku</th>
                    <th class="details-h"> Imagine</th>
                    <th class="details-h"> Nume produs</th>
                    <th class="details-h quantities"> Cantitate</th>
                    <th class="details-h"> Pret unitar</th>
                    <th class="details-h details-h-one"> Total</th>
                </tr>
                <!-- first item -->
                @foreach($req as $key => $product)
                    <tr class="first-item-row">
                        <td class="sold-item-values">
                            <span class="product-number">#{{$product['sku']}} </span>
                        </td>
                        <td class="sold-item-values">
                            <img width="100px" src="{{'https://cezomobila.md'.$product['image']}}" class="product-number"/>
                        </td>
                        <td class="sold-item-values">
                            <span class="product-number"><a target="_blank" href="{{'https://cezomobila.md/product/'.$product['slug']}}">{{$product['name']}}</a> </span>
                        </td>
                        <td class="sold-item-values sold-item-quantity"> {{$product['quantity']}} </td>
                        <td class="sold-item-values sold-item-price"> {{$product['price']}} Lei</td>
                        <td class="sold-item-values"> {{$product['total']}} Lei</td>
                    </tr>
                @endforeach

            </table>
        </div>

        <!-- PAYMENT RECAP HERE -->
        <div class="payment-container">
            <div class="payment-recap col-2">
                <p class="payment-p"> Pret livrare: </p>
            </div>
            <!-- second column -->
            <div class="payment-recap col-2">
                <p class="payment-p total bold"> {{$user_info['delivery_price']}} Lei </p>
            </div>
            <div class="payment-recap col-2">
                <p class="payment-p"> <strong>Total:</strong> </p>
            </div>
            <!-- second column -->
            <div class="payment-recap col-2">
                <p class="payment-p total bold"> {{$total}} Lei </p>
            </div>
        </div>

        <!-- DELIVERY ADDRESS + INVOICE DETAILS -->
        <div class="delivery-container">
            <!-- delivery column-->
            <div class="delivery-col-left">
                <h4 class="delivery-heading"> Date personale </h4>
                <p class="delivery-p">
                    {{$user_info['name']}}<br>
                    {{$user_info['company']}} <br>
                    MD, {{$user_info['local']['lang']['name']}}, {{$user_info['address']}} <br>
                    {{$user_info['phone']}}<br>
                    {{$user_info['email']}}<br>
                    {{$user_info['message']}}

                </p>
            </div>
        </div>
    </div>
</div>
</body>
</html>
