import Vuex from 'vuex';
import VuexPersist from 'vuex-persist';
import Vue from 'vue';
import {Notification} from 'element-ui'
import createPersistedState from 'vuex-persistedstate';

Vue.prototype.$notify = Notification;

Vue.use(Vuex);
const vuexLocalStorage = new VuexPersist({
    key: 'vuex',
    // The key to store the state on in the storage provider.
    storage: window.localStorage, // or window.sessionStorage or localForage
    // Function that passes the state and returns the state with only the objects you want to store.
    // reducer: state => state,
    // Function that passes a mutation and lets you decide if it should update the state in localStorage.
    // filter: mutation => (true)
})

export default new Vuex.Store({
    plugins: [createPersistedState({
        storage: window.sessionStorage,
    })],

    state: {
        cart: [],
        product: [],
        compares: [],
    },

    getters: {
        availableProducts(state, getters) {

        },

        cartProducts(state) {
            return state.cart.map(cartItem => {
                const product = state.product.find(product => product.id === cartItem.id)
                return {
                    id: cartItem.id,
                    quantity: cartItem.quantity,
                    color_id: cartItem.color_id,
                    price: cartItem.price,
                }
            })
        },

        cartTotal(state, getters) {
            return getters.cartProducts.reduce((total, product) => total + product.price * product.quantity, 0)
        },

        countCart(statem, getters) {
            return getters.cartProducts.reduce((total, product) => total + product.quantity, 0)
        },
    },

    actions: {
        addProductToCart(context, product) {
            const cartItem = context.state.cart.find(item => item.id === product.id)
            if (!cartItem) {
                Notification.success({
                    title: window.lang === 'ro' ? 'Succes' : 'Обновлено',
                    message: window.lang === 'ro' ? 'Produsul a fost adaugat in cos cu success' : 'Товар успешно добавлен в корзину',
                    type: 'success',
                    position: 'bottom-right'
                });
                context.commit('pushProductToCart', product)
            } else {
                let cart = {
                    item: cartItem,
                    quantity: product.quantity,
                    color_id: cartItem.color_id,
                    price: cartItem.price,
                }
                Notification.success({
                    title: window.lang === 'ro' ? 'Succes' : 'Обновлено',
                    message: window.lang === 'ro' ? 'Produsul a fost actualizat cu success' : 'Товар был успешно обновлен',
                    type: 'success',
                    position: 'bottom-right'
                });
                context.commit('udpateCartItem', cart)
            }

        },

        incrementItemQuantity(context, product) {
            if (product.quantity > 0) {
                const cartItem = context.state.cart.find(item => item.id === product.id)
                if (!cartItem) {
                    context.commit('pushProductToCart', product)
                } else {

                    context.commit('incrementItemQuantity', cartItem)
                }
            }
        },

        decrementProductInventory(context, product) {
            const cartItem = context.state.cart.find(item => item.id === product.id)
            if (cartItem.quantity > 1) {
                if (cartItem) {
                    context.commit('decrementProductInventory', cartItem)
                }
            } else {
                context.commit('deleteItem', cartItem);
            }
        },

        deleteFromStore(context, product) {
            let i = context.state.cart.map(item => item.id).indexOf(product.id) // find index of your object
            context.commit('delItm', i);
        },

        resetCartStore(context) {
            context.commit('resetCart');
        },
    },

    mutations: {

        pushProductToCart(state, product) {
            state.cart.push({
                id: product.id,
                quantity: product.quantity,
                color_id: product.color_id,
                price: product.price,
            })
        },

        udpateCartItem(state, cartItem) {
            cartItem.item.quantity = cartItem.quantity;
        },

        incrementItemQuantity(state, cartItem) {
            cartItem.quantity += cartItem.quantity;
        },

        decrementProductInventory(state, cartItem) {
            cartItem.quantity--
        },

        deleteItem(state, cartItem) {
            state.cart.splice(state.cart.indexOf(cartItem), 1);
        },

        delItm(state, i) {
            state.cart.splice(i, 1);
        },

        resetCart(state) {
            state.cart = [];
        }
    }

});
