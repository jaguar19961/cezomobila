/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
// require('./main');
import store from './store';

window.Vue = require('vue').default;

import ElementUI from 'element-ui'
import locale from 'element-ui/lib/locale/lang/en'
import 'element-ui/lib/theme-chalk/index.css';

Vue.use(ElementUI, {locale})

import {VueEditor} from "vue2-editor";


/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))
Vue.component('product-slider', require('./components/SliderProduct.vue').default);
Vue.component('catalog-filter', require('./components/CatalogFilter.vue').default);
Vue.component('new-search-bar', require('./components/NewSearchBar.vue').default);


Vue.component('admin-products', require('./components/admin/Products.vue').default);
Vue.component('admin-product-spec-item', require('./components/admin/ProductSpecItem.vue').default);
Vue.component('admin-edit-products', require('./components/admin/EditProduct.vue').default);
Vue.component('admin-assign-attribute', require('./components/admin/CreateAttributes.vue').default);
Vue.component('admin-create-credit', require('./components/admin/CreateCredits.vue').default);
Vue.component('admin-combine-products', require('./components/admin/CombineProducts.vue').default);
Vue.component('admin-upsells-module', require('./components/admin/AdminUpsellsModule.vue').default);

Vue.component('pagination', require('laravel-vue-pagination'));

// ui
Vue.component('search-bar', require('./components/front/ui/SearchBar.vue').default);
Vue.component('search-bar-dinamic', require('./components/front/ui/SearchBarDinamic.vue').default);
Vue.component('search-bar-desktop', require('./components/front/ui/SearchBarDesktop.vue').default);
Vue.component('categories', require('./components/front/ui/Categories.vue').default);
Vue.component('credit', require('./components/front/ui/Credit.vue').default);
Vue.component('mobile-categories', require('./components/front/ui/MobileCategories.vue').default);
Vue.component('header-menu', require('./components/front/ui/HeaderMenu.vue').default);
Vue.component('app-image', require('./components/front/ui/AppImage.vue').default);

//cart
Vue.component('cart-count', require('./components/front/ui/CartCount.vue').default);
Vue.component('cart', require('./components/front/pages/Cart.vue').default);
Vue.component('cart-item', require('./components/front/cart/Item.vue').default);
Vue.component('cart-delivery', require('./components/front/pages/Delivery.vue').default);

// products
Vue.component('product-item', require('./components/front/product/Item.vue').default);
Vue.component('upsells', require('./components/front/upsells/Upsells.vue').default);

//reviews
Vue.component('review-item', require('./components/front/reviews/Item.vue').default);
Vue.component('review-form', require('./components/front/reviews/Form.vue').default);

//pages
Vue.component('catalog', require('./components/front/pages/Catalog.vue').default);
Vue.component('product', require('./components/front/product/Product.vue').default);


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    store,
    data() {
        return {
            showMore: false,
            menu: false,
            mob_nav: false
        }
    }
});
