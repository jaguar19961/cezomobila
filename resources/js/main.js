const searchInput = document.querySelector(".search_bar input");
document.getElementById("nav_trigger").addEventListener("click", (event) => {
    event.preventDefault();
    document.getElementById("cezo_submenu").classList.toggle("open");
});

searchInput.addEventListener("focus", () => {
    document.querySelector(".search_dropdown").classList.add("open");
});
searchInput.addEventListener("blur", () => {
    document.querySelector(".search_dropdown").classList.remove("open");
});

document.getElementById("mob_nav_trigger").addEventListener("click", (ev) => {
    document.querySelector(".mob_sub_menu").classList.add("active");
});

document.getElementById("mob_nav_close").addEventListener("click", (ev) => {
    document.querySelector(".mob_sub_menu").classList.remove("active");
});

document.querySelector("#credit").addEventListener("click", (event) => {
    event.preventDefault();
    var modal = event.currentTarget.dataset["modal"];
    document.querySelector(modal).classList.add("open");
    document.querySelector(".overlay").style.display = "block";
});

document.querySelector("#credit_modal__close").addEventListener("click", (event) => {
    event.preventDefault();
    document.querySelector(".credit_modal").classList.remove("open");
    document.querySelector(".overlay").style.display = "none";
});
