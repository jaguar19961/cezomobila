<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCreditFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('credit_forms', function (Blueprint $table) {
            $table->id();
            $table->text('product');
            $table->integer('product_id');
            $table->integer('quantity');
            $table->integer('credit_id');
            $table->double('avans');
            $table->text('name');
            $table->text('phone');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('credit_forms');
    }
}
